package com.mobile.cti.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.andexert.library.RippleView;
import com.mobile.cti.android.R;
import com.bumptech.glide.Glide;

import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by Dirga on 7/22/2016.
 */
public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ItemView> {
    private String[] text = new String[6];
    private int[] icons = new int[]{
            R.drawable.ic_promo,
            R.drawable.ic_address,
            R.drawable.ic_news,
            R.drawable.ic_product,
            R.drawable.ic_video,
            R.drawable.ic_calculator
    };
    private Context context;
    private ItemView.OnClickItem onClickItem;

    public MainAdapter(Context context) {
        this.context = context;
        initText();
    }

    private void initText() {
        text[0] = "PROMO";
        text[1] = "COMPANY";
        text[2] = "ARTICLES";
        text[3] = "PRODUCTS";
        text[4] = "GALLERY";
        text[5] = "CALCULATOR";
    }

    @Override
    public ItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_group_main_item, parent, false);
        return new ItemView(itemView, context);
    }

    @Override
    public void onBindViewHolder(ItemView holder, int position) {
        holder.setOnClickItem(onClickItem);
        holder.bind(icons[position], text[position], position);
    }

    @Override
    public int getItemCount() {
        return icons.length;
    }

    public void setOnClickItem(ItemView.OnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    public static class ItemView extends RecyclerView.ViewHolder {
        Context context;
        ISCTextView title;
        ImageView imageView;
        RippleView container;
        OnClickItem onClickItem;

        public ItemView(View itemView, Context context) {
            super(itemView);
            if (itemView != null) {
                container = (RippleView) itemView.findViewById(R.id.rl_item_main_container);
                title = (ISCTextView) itemView.findViewById(R.id.tv_main_item);
                imageView = (ImageView) itemView.findViewById(R.id.iv_main_item);
                this.context = context;
            }
        }

        public void bind(int icon, String text, final int position) {
            Glide.with(context).load(icon).into(imageView);
            title.setText(text);
//            container.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });

            container.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
                @Override
                public void onComplete(RippleView rippleView) {
                    onClickItem.onItemClickListener(position);
                }
            });
        }

        public void setOnClickItem(OnClickItem onClickItem) {
            this.onClickItem = onClickItem;
        }

        public interface OnClickItem {
            void onItemClickListener(int position);
        }
    }
}
