package com.mobile.cti.android.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dirga on 8/11/2016.
 */
public class Product extends BaseEntities {
    @SerializedName("data")
    @Expose
    private List<ProductImpl> data = new ArrayList<ProductImpl>();
    /**
     *
     * @return
     * The data
     */
    public List<ProductImpl> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<ProductImpl> data) {
        this.data = data;
    }
}
