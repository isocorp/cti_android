package com.mobile.cti.android.views;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.mobile.cti.android.R;
import com.mobile.cti.android.adapter.GenericNewsAdapter;
import com.mobile.cti.android.entities.GenericNews;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dirga on 27/07/16.
 */
public class GenericNewsLayout extends LinearLayout implements GenericNewsAdapter.ItemView.OnItemClickListener{
    @BindView(R.id.list_generic_news)
    RecyclerView listGenericNews;
    private GenericNewsAdapter adapter;
    private onItemNewsClickListener onItemNewsClickListener;

    public GenericNewsLayout(Context context) {
        super(context);
        init();
    }

    public GenericNewsLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GenericNewsLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View inflaterView = inflater.inflate(R.layout.view_group_generic_news, this, true);
        ButterKnife.bind(inflaterView, this);
        adapter = new GenericNewsAdapter(getContext());
        adapter.setOnItemClickListener(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        listGenericNews.setLayoutManager(mLayoutManager);
        listGenericNews.setItemAnimator(new DefaultItemAnimator());
        listGenericNews.setAdapter(adapter);
    }

    public void setData(List<GenericNews> genericNewses){
        adapter.setData(genericNewses);
    }

    public void setOnItemNewsClickListener(GenericNewsLayout.onItemNewsClickListener onItemNewClickListener) {
        this.onItemNewsClickListener = onItemNewClickListener;
    }

    @Override
    public void onItemClick(String id) {
        if (onItemNewsClickListener != null){
            onItemNewsClickListener.onItemClicked(id);
        }
    }

    public interface onItemNewsClickListener{
        void onItemClicked(String id);
    }
}
