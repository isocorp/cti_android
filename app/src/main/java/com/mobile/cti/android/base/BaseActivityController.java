package com.mobile.cti.android.base;


import android.app.Activity;

import butterknife.ButterKnife;

/**
 * Created by Dirga on 08/03/2015.
 */
public class BaseActivityController <T extends Activity> {
    protected T activity;

    public BaseActivityController(T t){
        this.activity   = t;
        setActivity(t);
        ButterKnife.bind(this,t);
    }

    public T getActivity(){
        return activity;
    }

    public void setActivity(T activity1){
        this.activity   = activity1;
    }

}
