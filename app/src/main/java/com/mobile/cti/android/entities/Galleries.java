package com.mobile.cti.android.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dirga on 8/12/2016.
 */
public class Galleries extends BaseEntities {
    @SerializedName("data")
    @Expose
    private List<GalleriesImpl> data = new ArrayList<GalleriesImpl>();

    /**
     *
     * @return
     * The data
     */
    public List<GalleriesImpl> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<GalleriesImpl> data) {
        this.data = data;
    }
}
