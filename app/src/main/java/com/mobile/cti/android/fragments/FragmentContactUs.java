package com.mobile.cti.android.fragments;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mobile.cti.android.R;
import com.mobile.cti.android.adapter.ContactUsItemAdapter;
import com.mobile.cti.android.entities.ContactPerson;
import com.mobile.cti.android.entities.ContactUs;
import com.mobile.cti.android.entities.ContactUsImpl;
import com.mobile.cti.android.rest.AppRestController;
import com.mobile.cti.android.rest.AppRestService;
import com.mobile.cti.android.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dirga on 25/07/16.
 */
public class FragmentContactUs extends Fragment implements ContactUsItemAdapter.ItemView.OnItemClicked {
    @BindView(R.id.pb_contact_us)
    ProgressBar pbContactUs;
    private AppRestService mRestService = AppRestController.getInstance().getAppRestService();
    @BindView(R.id.rv_fragment_contact_us)
    RecyclerView rvFragmentContactUs;
    private ContactUsItemAdapter adapter;
    private int position;
    List<ContactPerson> contactPersons = new ArrayList<>();

    public static FragmentContactUs newInstance(int position) {
        FragmentContactUs fragmentContactUs = new FragmentContactUs();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.CONTACT_POSITION, position);
        fragmentContactUs.setArguments(bundle);
        return fragmentContactUs;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ContactUsItemAdapter(getActivity());
        adapter.setOnItemClicked(this);
        if (getArguments() != null) {
            position = getArguments().getInt(Constants.CONTACT_POSITION);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        ButterKnife.bind(this, view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvFragmentContactUs.setLayoutManager(layoutManager);
        rvFragmentContactUs.setItemAnimator(new DefaultItemAnimator());
        rvFragmentContactUs.setAdapter(adapter);
        setData(position);
        return view;
    }

    public void setData(int position) {
        switch (position) {
            case 0:
                mRestService.getCustomerService(contactUsCallback);
                break;
            case 1:
                mRestService.getSales(contactUsCallback);
                break;
            case 2:
                mRestService.getDirector(contactUsCallback);
                break;
        }
    }

    Callback<ContactUs> contactUsCallback = new Callback<ContactUs>() {
        @Override
        public void success(ContactUs contactUs, Response response) {
            pbContactUs.setVisibility(View.GONE);
            contactPersons.clear();
            onSuccess(contactUs);
        }

        @Override
        public void failure(RetrofitError error) {
            pbContactUs.setVisibility(View.GONE);
            onFail();
        }
    };

    private void onSuccess(ContactUs contactUs) {
        if (contactUs != null) {
            for (int i = 0; i < contactUs.getData().size(); i++) {
                ContactPerson contactPerson = new ContactPerson();
                ContactUsImpl contactUsImp = contactUs.getData().get(i);
                contactPerson.setEmail(contactUsImp.getEmail());
                contactPerson.setWhatsApp(contactUsImp.getWhatsapp());
                contactPerson.setPhone(contactUsImp.getPhone());
                contactPerson.setLine(contactUsImp.getLine());
                contactPerson.setName(contactUsImp.getPosition() + " " + contactUsImp.getName());
                contactPerson.setImage(contactUsImp.getBaseUrl() + "/300/" + contactUsImp.getFileOriginal());
                contactPerson.setBbm(contactUsImp.getBbm());
                contactPersons.add(contactPerson);
            }
            adapter.setData(contactPersons);
        }
    }

    private void onFail() {
        Toast.makeText(getActivity(), "Terjadi kesalahan", Toast.LENGTH_LONG).show();
    }

//    private void setSales() {
//        List<ContactPerson> contactPersons = new ArrayList<>();
//        ContactPerson contactPerson2 = new ContactPerson();
//        contactPerson2.setEmail("salessupport@ptcti.com");
//        contactPerson2.setWhatsApp("088217053307");
//        contactPerson2.setPhone("081230731001");
//        contactPerson2.setLine("salessupportcti");
//        contactPerson2.setName("Sales Support: Rini");
//        contactPerson2.setImage(R.drawable.sales_rini);
//        contactPerson2.setBbm("D25C0427");
//        contactPersons.add(contactPerson2);
//
//        ContactPerson contactPerson = new ContactPerson();
//        contactPerson.setEmail("abdu@ptcti.com");
//        contactPerson.setWhatsApp("08885201667");
//        contactPerson.setPhone("08113119810");
////        contactPerson.setLine("08111122xxxx");
//        contactPerson.setName("Technical Sales: Abdu");
//        contactPerson.setImage(R.drawable.sales_abdu);
//        contactPerson.setBbm("5D472FC6");
//        contactPersons.add(contactPerson);
//
//        ContactPerson contactPerson3 = new ContactPerson();
//        contactPerson3.setEmail("mardi@cti.co.id");
//        contactPerson3.setWhatsApp("08885201665");
//        contactPerson3.setPhone("082243880011");
////        contactPerson3.setLine("08111122xxxx");
//        contactPerson3.setName("Technical Sales: Mardi");
//        contactPerson3.setImage(R.drawable.sales_dwi);
//        contactPerson3.setBbm("5D23A28A");
//        contactPersons.add(contactPerson3);
//
//        adapter.setData(contactPersons);
//    }
//
//    private void setContactSupport() {
//        List<ContactPerson> contactPersons = new ArrayList<>();
//
//        ContactPerson contactPerson3 = new ContactPerson();
//        contactPerson3.setEmail("cs@ptcti.com");
//        contactPerson3.setWhatsApp("081232771001");
//        contactPerson3.setPhone("08819059100");
//        contactPerson3.setLine("csptcti");
////        contactPerson3.setLine("08111122xxxx");
//        contactPerson3.setName("CS:Etha");
//        contactPerson3.setImage(R.drawable.cs_etha);
//        contactPerson3.setBbm("760AACE3");
//        contactPersons.add(contactPerson3);
//
//        ContactPerson contactPerson = new ContactPerson();
//        contactPerson.setEmail("franda.cti@gmail.com");
//        contactPerson.setWhatsApp("088217561005");
//        contactPerson.setPhone("088217561005");
//        contactPerson.setLine("adm.ptcti");
//        contactPerson.setName("Admin:Nanda");
//        contactPerson.setImage(R.drawable.cs_nanda);
////        contactPerson.setBbm("545739E4");
//        contactPersons.add(contactPerson);
//
//        ContactPerson contactPerson2 = new ContactPerson();
//        contactPerson2.setEmail("arywidyaningrum.cti@gmail.com");
////        contactPerson2.setWhatsApp("08111122xxxx");
//        contactPerson2.setPhone("085746302736");
////        contactPerson2.setLine("08111122xxxx");
//        contactPerson2.setName("Finance:Ary");
//        contactPerson2.setImage(R.drawable.cs_ary);
//        contactPerson2.setBbm("536546E6");
//        contactPersons.add(contactPerson2);
//
//        adapter.setData(contactPersons);
//    }
//
//    private void setDirectors() {
//        List<ContactPerson> contactPersons = new ArrayList<>();
//        ContactPerson contactPerson = new ContactPerson();
//        contactPerson.setEmail("belidictipastiuntung@gmail.com");
//        contactPerson.setWhatsApp("08165429463");
//        contactPerson.setPhone("082131470809");
////        contactPerson.setLine("08111122xxxx");
//        contactPerson.setName("Sales Director: Soegiarto Santoso, ST");
//        contactPerson.setImage(R.drawable.director_soegiarto_santoso);
//        contactPerson.setBbm("59DF5829");
//        contactPersons.add(contactPerson);
//
//        ContactPerson contactPerson2 = new ContactPerson();
//        contactPerson2.setEmail("tonny@ptcti.com");
//        contactPerson2.setWhatsApp("08123538909");
//        contactPerson2.setPhone("08885201996");
//        contactPerson2.setLine("tonnycti");
//        contactPerson2.setName("Marketing Director: Ir.Tonny Hartono");
//        contactPerson2.setImage(R.drawable.director_tonny);
//        contactPerson2.setBbm("24DA7B3D");
//        contactPersons.add(contactPerson2);
//
//        adapter.setData(contactPersons);
//    }


    @Override
    public void onWhatAppsclicked(String whatApps, String name) {
        if (!contactExists(getActivity(), whatApps)) {
            insertContact(whatApps, name);
        } else {
            sendToChat(whatApps);
        }
    }

    private void sendToChat(String whatApps) {
        Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage(Constants.PACKAGE_NAME_WHATSAPP);
        if (launchIntent != null) {
//            startActivity(launchIntent);
            String chatNumber = getContactIDFromNumber(whatApps,getActivity()) + "@s.whatsapp.net";
            Uri uri = Uri.parse("smsto:" + chatNumber);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.setPackage(Constants.PACKAGE_NAME_WHATSAPP);
            startActivity(Intent.createChooser(i, ""));
        } else {
            Toast.makeText(getActivity(), "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
        }
    }

    private String getContactIDFromNumber(String contactNumber, Context context) {
        contactNumber = Uri.encode(contactNumber);
        int phoneContactID = new Random().nextInt();
        Cursor contactLookupCursor = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, contactNumber), new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID}, null, null, null);
        while (contactLookupCursor.moveToNext()) {
            phoneContactID = contactLookupCursor.getInt(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
        }
        contactLookupCursor.close();
        return String.valueOf(phoneContactID);
    }

    @Override
    public void onLineClicked(String line) {
        Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage(Constants.PACKAGE_NAME_LINE);
        if (launchIntent != null) {
            startActivity(launchIntent);
        } else {
            Toast.makeText(getActivity(), "Line not Installed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBbmClicked(String bbm) {
        Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage(Constants.PACKAGE_NAME_BBM);
        if (launchIntent != null) {
            startActivity(launchIntent);
        } else {
            Toast.makeText(getActivity(), "Bbm not Installed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onEmailClicked(String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + email));
        startActivity(Intent.createChooser(emailIntent, "Send email"));
    }

    @Override
    public void onPhoneClicked(String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
        startActivity(intent);
    }

    private int getPackageInfo(String packageName) {
        try {
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo(packageName, 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            //Handle exception
        }
        return -1;
    }

    private void insertContact(String whatApps, String name) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        int rawContactInsertIndex = ops.size();

        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name) // Name of the person
                .build());
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(
                        ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, whatApps) // Number of the person
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE).build()); // Type of mobile number
        try {
            ContentProviderResult[] res = getActivity().getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            sendToChat(whatApps);
        } catch (RemoteException e) {
            // error
        } catch (OperationApplicationException e) {
            // error
        }
    }

    private boolean contactExists(Context context, String number) {
        Uri lookupUri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));
        String[] mPhoneNumberProjection = {ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME};
        Cursor cur = context.getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);
        try {
            if (cur.moveToFirst()) {
                return true;
            }
        } finally {
            if (cur != null)
                cur.close();
        }
        return false;
    }

}
