package com.mobile.cti.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.mobile.cti.android.R;
import com.mobile.cti.android.entities.GenericNews;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.ArrayList;
import java.util.List;

import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by dirga on 27/07/16.
 */
public class GenericNewsAdapter extends RecyclerView.Adapter<GenericNewsAdapter.ItemView> {
    private Context context;
    private List<GenericNews> genericNewsList = new ArrayList<>();
    private ItemView.OnItemClickListener onItemClickListener;
    private String url = "https://photos-4.dropbox.com/t/2/AAD35lXCk33X5Rp7gl-h7zG9nfejD2n7_3bvuZDTchNxPw/12/554951631/jpeg/32x32/3/1470135600/0/2/product.jpg/EO6yg7kEGAkgAigC/Fb6BDnSWrlA7P2QzgpDamqUl4b_lngLfncwP9woTOFU?size_mode=3&size=1280x960";

    public GenericNewsAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<GenericNews> genericNewsList) {
        this.genericNewsList = genericNewsList;
        notifyDataSetChanged();
    }

    public void setDummyData() {
        GenericNews genericNews = new GenericNews();
        genericNews.setId("1");
        genericNews.setTitle("Helo Dirga");
        genericNews.setDate("12 Juli 2016");
        genericNews.setCommentCount("10");
        genericNews.setImage(url);
        genericNewsList.add(genericNews);

        GenericNews genericNews2 = new GenericNews();
        genericNews2.setId("2");
        genericNews2.setTitle("Helo CTI");
        genericNews2.setDate("12 Juli 2016");
        genericNews2.setCommentCount("100");
        genericNews2.setImage(url);
        genericNewsList.add(genericNews2);

        GenericNews genericNews3 = new GenericNews();
        genericNews3.setId("3");
        genericNews3.setTitle("Helo Dirga dan CTI");
        genericNews3.setDate("12 Juli 2016");
        genericNews3.setCommentCount("101");
        genericNews3.setImage(url);
        genericNewsList.add(genericNews3);

        GenericNews genericNews4 = new GenericNews();
        genericNews4.setId("4");
        genericNews4.setTitle("Helo Isocorp");
        genericNews4.setDate("12 Juli 2016");
        genericNews4.setCommentCount("1000");
        genericNews4.setImage(url);
        genericNewsList.add(genericNews4);
        notifyDataSetChanged();
    }

    @Override
    public ItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_group_item_news, parent, false);
        return new ItemView(itemView, context);
    }

    @Override
    public void onBindViewHolder(ItemView holder, int position) {
        holder.setOnItemClickListener(onItemClickListener);
        holder.bind(genericNewsList.get(position));
    }

    @Override
    public int getItemCount() {
        return genericNewsList.size();
    }

    public void setOnItemClickListener(ItemView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public static class ItemView extends RecyclerView.ViewHolder {
        Context context;
        ImageView imageNews;
        ISCTextView tvTitleNews;
        ISCTextView tvDateNews;
        OnItemClickListener onItemClickListener;
        ProgressBar pbGenericNews;
        RelativeLayout containerGenericNews;

        public ItemView(View itemView, Context context) {
            super(itemView);
            if (itemView != null){
                this.context = context;
                imageNews = (ImageView)itemView.findViewById(R.id.iv_generic_news);
                tvTitleNews = (ISCTextView)itemView.findViewById(R.id.title_generic_news);
                tvDateNews = (ISCTextView)itemView.findViewById(R.id.tv_generic_news_date);
                pbGenericNews = (ProgressBar)itemView.findViewById(R.id.pb_generic_news);
                containerGenericNews = (RelativeLayout)itemView.findViewById(R.id.container_generic_news);
            }
        }

        public void bind(final GenericNews genericNews) {
            if (genericNews != null) {
                if (genericNews.getImage() != null){
                    Glide.with(context).load(genericNews.getImage()).into(new SimpleTarget<GlideDrawable>() {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                            imageNews.setImageDrawable(resource);
                            pbGenericNews.setVisibility(View.GONE);
                        }
                    });
                }
                tvTitleNews.setText(genericNews.getTitle());
                tvDateNews.setText(genericNews.getDate());
                containerGenericNews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onItemClickListener != null){
                            onItemClickListener.onItemClick(genericNews.getId());
                        }
                    }
                });
            }
        }

        public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }

        public interface OnItemClickListener{
            void onItemClick(String id);
        }
    }
}
