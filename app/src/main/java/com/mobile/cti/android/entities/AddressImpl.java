package com.mobile.cti.android.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dirga on 8/22/2016.
 */
public class AddressImpl {
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("telp")
    @Expose
    private String telp;
    @SerializedName("whatsapp")
    @Expose
    private String whatsapp;
    @SerializedName("fax")
    @Expose
    private String fax;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("pdf_link")
    @Expose
    private String pdfLink;

    /**
     *
     * @return
     * The address
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The telp
     */
    public String getTelp() {
        return telp;
    }

    /**
     *
     * @param telp
     * The telp
     */
    public void setTelp(String telp) {
        this.telp = telp;
    }

    /**
     *
     * @return
     * The whatsapp
     */
    public String getWhatsapp() {
        return whatsapp;
    }

    /**
     *
     * @param whatsapp
     * The whatsapp
     */
    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    /**
     *
     * @return
     * The fax
     */
    public String getFax() {
        return fax;
    }

    /**
     *
     * @param fax
     * The fax
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The pdfLink
     */
    public String getPdfLink() {
        return pdfLink;
    }

    /**
     *
     * @param pdfLink
     * The pdf_link
     */
    public void setPdfLink(String pdfLink) {
        this.pdfLink = pdfLink;
    }
}
