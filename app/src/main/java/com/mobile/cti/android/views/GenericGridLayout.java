package com.mobile.cti.android.views;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.mobile.cti.android.R;
import com.mobile.cti.android.adapter.GenericGridAdapter;
import com.mobile.cti.android.entities.GenericGridData;
import com.mobile.cti.android.utils.GridSpacingItemDecoration;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dirga on 26/07/16.
 */
public class GenericGridLayout extends LinearLayout implements GenericGridAdapter.ItemView.OnItemClickListener {
    @BindView(R.id.generic_grid_layout)
    RecyclerView genericGridLayout;
    int spanCount = 3; // 3 columns
    int spacing = 10; // 50px
    boolean includeEdge = false;
    private GenericGridAdapter adapter;
    private OnItemClickListener onItemClickListener;

    public GenericGridLayout(Context context) {
        super(context);
        init();
    }

    public GenericGridLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GenericGridLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View inflaterView = inflater.inflate(R.layout.view_group_generic_grid, this, true);
        ButterKnife.bind(inflaterView, this);
        adapter = new GenericGridAdapter(getContext());
        adapter.setOnItemClickListener(this);
        GridLayoutManager mLayoutManager = new GridLayoutManager(getContext(), 3);
        genericGridLayout.setLayoutManager(mLayoutManager);
        genericGridLayout.setItemAnimator(new DefaultItemAnimator());
        genericGridLayout.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge, getContext()));
        genericGridLayout.setAdapter(adapter);
    }

    public void setDataGrid(List<GenericGridData> dataGrid) {
        adapter.setDataList(dataGrid);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void onItemClicked(String id, String type, String url, int position) {
        if (onItemClickListener != null) {
            onItemClickListener.onItemClicked(id, type, url ,position);
        }
    }

    public interface OnItemClickListener {
        void onItemClicked(String id, String type, String url, int position);
    }
}
