package com.mobile.cti.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mobile.cti.android.R;
import com.mobile.cti.android.activities.gallerydetail.GalleryDetailActivity;
import com.mobile.cti.android.entities.Galleries;
import com.mobile.cti.android.entities.GenericGridData;
import com.mobile.cti.android.rest.AppRestController;
import com.mobile.cti.android.rest.AppRestService;
import com.mobile.cti.android.utils.TransitionActivity;
import com.mobile.cti.android.views.GenericGridLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dirga on 26/07/16.
 */
public class FragmentPhotoVideo extends Fragment implements GenericGridLayout.OnItemClickListener {
    private static final String ID_POSITION = "position";
    @BindView(R.id.pb_galleries)
    ProgressBar pbGalleries;
    private AppRestService mRestService = AppRestController.getInstance().getAppRestService();
    @BindView(R.id.grid_photo_video)
    GenericGridLayout gridPhotoVideo;
    private int position;
    List<GenericGridData> dataGrid = new ArrayList<>();

    public static FragmentPhotoVideo newInstance(int position) {
        FragmentPhotoVideo fragmentPhotoVideo = new FragmentPhotoVideo();
        Bundle bundle = new Bundle();
        bundle.putInt(ID_POSITION, position);
        fragmentPhotoVideo.setArguments(bundle);
        return fragmentPhotoVideo;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt(ID_POSITION);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo_video, container, false);
        ButterKnife.bind(this, view);
        gridPhotoVideo.setOnItemClickListener(this);
        getGalleries();
//        gridPhotoVideo.setDataGrid();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void getGalleries() {
        switch (position) {
            case 0:
                mRestService.getGalleries(galleriesCallback);
                break;
            case 1:
                mRestService.getGalleriesImage(galleriesCallback);
                break;
            case 2:
                mRestService.getGalleriesVideo(galleriesCallback);
                break;
        }
    }

    Callback<Galleries> galleriesCallback = new Callback<Galleries>() {
        @Override
        public void success(Galleries galleries, Response response) {
            pbGalleries.setVisibility(View.GONE);
            onSuccess(galleries);
        }

        @Override
        public void failure(RetrofitError error) {
            pbGalleries.setVisibility(View.GONE);
            onFail(error);
        }
    };

    private void onSuccess(Galleries galleries) {
        dataGrid.clear();
        for (int i = 0; i < galleries.getData().size(); i++) {
            GenericGridData genericGridData = new GenericGridData();
            genericGridData.setId(String.valueOf(galleries.getData().get(i).getId()));
            genericGridData.setTitle(galleries.getData().get(i).getTitle());
            genericGridData.setImage(galleries.getData().get(i).getFileName());
            genericGridData.setType(galleries.getData().get(i).getType());
            genericGridData.setThumb(galleries.getData().get(i).getThumbnail());
            dataGrid.add(genericGridData);
        }
        gridPhotoVideo.setDataGrid(dataGrid);
    }

    private void onFail(RetrofitError error) {
        Toast.makeText(getActivity(), "Terjadi kesalahan", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClicked(String id, String type, String url, int position) {
        if (type.equals("image")){
            Intent intent = new Intent(getActivity(), GalleryDetailActivity.class);
            intent.putExtra(GalleryDetailActivity.ID_GALLERY,url);
            TransitionActivity.transitionRightToLeft(getActivity(),intent);
        }
    }
}
