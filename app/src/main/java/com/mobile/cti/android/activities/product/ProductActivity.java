package com.mobile.cti.android.activities.product;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.widget.ProgressBar;

import com.mobile.cti.android.R;
import com.mobile.cti.android.base.BaseActivity;
import com.mobile.cti.android.views.GenericGridLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by dirga on 26/07/16.
 */
public class ProductActivity extends BaseActivity {
    @BindView(R.id.product_container)
    CoordinatorLayout productContainer;
    @BindView(R.id.pb_product)
    ProgressBar pbProduct;
    private Toolbar toolbar;
    @BindView(R.id.grid_product)
    GenericGridLayout gridProduct;
    private ISCTextView titlePage;
    private ProductActivityController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new ProductActivityController(this);
//        gridProduct.setProduct();
    }

    @Override
    public Toolbar getToolbar() {
        setContentView(R.layout.activity_product);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar_product);
        titlePage = (ISCTextView) toolbar.findViewById(R.id.toolbar_title);
        titlePage.setText("Products");
        return toolbar;
    }

    @Override
    public String toolbarTitle() {
        return null;
    }

    @Override
    public String screenName() {
        return "Product Activity";
    }
}


