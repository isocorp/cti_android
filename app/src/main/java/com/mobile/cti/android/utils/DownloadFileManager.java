package com.mobile.cti.android.utils;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import java.io.File;

/**
 * Created by Dirga on 8/12/2016.
 */
public class DownloadFileManager {
    private static DownloadFileManager instance;
    private Context context;
    protected DownloadManager downloadManagerAndroid;

    public static DownloadFileManager getInstance() {
        if (instance == null) {
            instance = new DownloadFileManager();
        }
        return instance;
    }

    public DownloadFileManager setContext(Context context){
        this.context = context;
        return this;
    }

    public DownloadFileManager showDownloadStatus(String urlDownloadFile, String name) {
//        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + Environment.DIRECTORY_PICTURES;
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!path.exists()){
            path.mkdir();
        }
        downloadManagerAndroid = (DownloadManager) context.getSystemService(context.DOWNLOAD_SERVICE);
        Uri downloadUri = Uri.parse(urlDownloadFile);
        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
        request.setTitle("Downloading file");
        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Downloading file")
                .setDescription(name)
                .setDestinationInExternalPublicDir(path.getAbsolutePath(), name);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            request.setShowRunningNotification(true);
        } else {
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        downloadManagerAndroid.enqueue(request);
        return this;
    }
}
