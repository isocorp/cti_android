package com.mobile.cti.android.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dirga on 8/11/2016.
 */
public class Promo extends BaseEntities {
    @SerializedName("data")
    @Expose
    private List<PromoImpl> data = new ArrayList<PromoImpl>();

    /**
     *
     * @return
     * The data
     */
    public List<PromoImpl> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<PromoImpl> data) {
        this.data = data;
    }
}
