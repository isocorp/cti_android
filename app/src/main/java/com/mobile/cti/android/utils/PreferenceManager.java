package com.mobile.cti.android.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Dirga on 7/22/2016.
 */
public class PreferenceManager {
    public static void setToken(Context context, String token) {
        SharedPreferences shared = android.preference.PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(Constants.PREFERENCE_TOKEN, token);
        editor.apply();
    }

    public static String getToken(Context context) {
        try {
            SharedPreferences shared = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
            String value = shared.getString(Constants.PREFERENCE_TOKEN, null);
            return value;
        } catch (Exception e) {
//            Util.log(context.getString(R.string.ft_error_log_savepreferenceinfo));
        }
        return null;
    }

    public static void setID(Context context, String token) {
        SharedPreferences shared = android.preference.PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(Constants.PREFERENCE_ID, token);
        editor.apply();
    }

    public static String getID(Context context) {
        try {
            SharedPreferences shared = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
            String value = shared.getString(Constants.PREFERENCE_ID, null);
            return value;
        } catch (Exception e) {
//            Util.log(context.getString(R.string.ft_error_log_savepreferenceinfo));
        }
        return null;
    }

    public static void setGcmToken(Context context, String token) {
        SharedPreferences shared = android.preference.PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(Constants.PREFERENCE_TOKEN_GCM, token);
        editor.apply();
    }

    public static String getGcmToken(Context context) {
        try {
            SharedPreferences shared = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
            String value = shared.getString(Constants.PREFERENCE_TOKEN_GCM, null);
            return value;
        } catch (Exception e) {
//            Util.log(context.getString(R.string.ft_error_log_savepreferenceinfo));
        }
        return null;
    }

    public static void setEndDate(Context context, String token) {
        SharedPreferences shared = android.preference.PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(Constants.PREFERENCE_END_DATE, token);
        editor.apply();
    }

    public static String getEndDate(Context context) {
        try {
            SharedPreferences shared = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
            String value = shared.getString(Constants.PREFERENCE_END_DATE, null);
            return value;
        } catch (Exception e) {
//            Util.log(context.getString(R.string.ft_error_log_savepreferenceinfo));
        }
        return null;
    }

    public static void setFacebookId(Context context, String token) {
        SharedPreferences shared = android.preference.PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(Constants.FACEBOOK_ID, token);
        editor.apply();
    }

    public static String getFacebookId(Context context) {
        try {
            SharedPreferences shared = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
            String value = shared.getString(Constants.FACEBOOK_ID, null);
            return value;
        } catch (Exception e) {
//            Util.log(context.getString(R.string.ft_error_log_savepreferenceinfo));
        }
        return null;
    }
}
