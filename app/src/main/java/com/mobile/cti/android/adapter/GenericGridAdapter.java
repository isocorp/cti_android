package com.mobile.cti.android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.mobile.cti.android.R;
import com.mobile.cti.android.entities.GenericGridData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.FileDescriptorBitmapDecoder;
import com.bumptech.glide.load.resource.bitmap.VideoBitmapDecoder;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.ArrayList;
import java.util.List;

import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by dirga on 26/07/16.
 */
public class GenericGridAdapter extends RecyclerView.Adapter<GenericGridAdapter.ItemView> {
    private Context context;
    private List<GenericGridData> dataList = new ArrayList<>();
    private ItemView.OnItemClickListener onItemClickListener;

    public GenericGridAdapter(Context context) {
        this.context = context;
    }

    public void setDataList(List<GenericGridData> dataList) {
        this.dataList.clear();
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    @Override
    public ItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_group_item_generic_grid, parent, false);
        return new ItemView(itemView, context);
    }

    @Override
    public void onBindViewHolder(ItemView holder, int position) {
        holder.setOnItemClickListener(onItemClickListener);
        holder.setData(dataList.get(position), position);
    }

    public void setOnItemClickListener(ItemView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public static class ItemView extends RecyclerView.ViewHolder {
        Context context;
        ISCTextView title;
        ImageView imageView;
        OnItemClickListener onItemClickListener;
        LinearLayout containerGrid;
        ProgressBar progressBar;
        LinearLayout containerTitle;
        ImageView fileVideo;
        ISCTextView viewCount;

        public ItemView(View itemView, Context context) {
            super(itemView);
            if (itemView != null) {
                this.context = context;
                title = (ISCTextView) itemView.findViewById(R.id.tv_item_title_generic_grid);
                imageView = (ImageView) itemView.findViewById(R.id.iv_item_generic_grid);
                containerGrid = (LinearLayout) itemView.findViewById(R.id.container_generic_grid);
                progressBar = (ProgressBar) itemView.findViewById(R.id.pb_generic_grid);
                containerTitle = (LinearLayout) itemView.findViewById(R.id.container_title);
                fileVideo = (ImageView) itemView.findViewById(R.id.iv_file_video);
                viewCount = (ISCTextView) itemView.findViewById(R.id.tv_page_count);
            }
        }

        protected void setData(final GenericGridData data, final int position) {
            if (data != null) {
                if (data.getTitle() != null) {
                    title.setText(data.getTitle());
                } else {
                    containerTitle.setVisibility(View.GONE);
                }

                viewCount.setText(data.getViewCount());

                if (data.getType() != null) {
                    if (data.getType().equals("image")) {
                        fileVideo.setVisibility(View.GONE);
                        if (data.getOriginalFileName() != null) {
                            Glide.with(context).load(data.getBaseUrl() + "/300/" + data.getOriginalFileName()).into(new SimpleTarget<GlideDrawable>() {
                                @Override
                                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                    imageView.setImageDrawable(resource);
                                    progressBar.setVisibility(View.GONE);
                                }
                            });
                        } else {
                            Glide.with(context).load(data.getImage()).into(new SimpleTarget<GlideDrawable>() {
                                @Override
                                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                    imageView.setImageDrawable(resource);
                                    progressBar.setVisibility(View.GONE);
                                }
                            });
                        }
                    } else {
                        Glide.with(context).load(data.getThumb()).into(new SimpleTarget<GlideDrawable>() {
                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                imageView.setImageDrawable(resource);
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                    }
                }
                containerGrid.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onItemClickListener.onItemClicked(data.getId(), data.getType(), data.getImage(), position);

                    }
                });

            }
        }

        public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }

        public interface OnItemClickListener {
            void onItemClicked(String id, String type, String url, int posistion);
        }
    }
}
