package com.mobile.cti.android.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mobile.cti.android.fragments.FragmentContactUs;

/**
 * Created by dirga on 25/07/16.
 */
public class ContactUsPagerAdapter extends FragmentPagerAdapter {
    private String[] titlePage = new String[3];

    public ContactUsPagerAdapter(FragmentManager fm) {
        super(fm);
        initTitle();
    }

    private void initTitle(){
        titlePage[0]="CS";
        titlePage[1]="Sales";
        titlePage[2]="Director";
    }

    @Override
    public Fragment getItem(int position) {
        return FragmentContactUs.newInstance(position);
    }

    @Override
    public int getCount() {
        return titlePage.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titlePage[position];
    }
}
