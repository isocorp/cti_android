package com.mobile.cti.android.activities.address;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.mobile.cti.android.R;
import com.mobile.cti.android.activities.viewcompro.ViewCompro;
import com.mobile.cti.android.base.BaseActivityController;
import com.mobile.cti.android.entities.Address;
import com.mobile.cti.android.rest.AppRestController;
import com.mobile.cti.android.rest.AppRestService;
import com.mobile.cti.android.utils.DownloadFileManager;
import com.mobile.cti.android.utils.TransitionActivity;

import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dirga on 06/08/16.
 */
public class AddressActivityController extends BaseActivityController<AddressActivity> {
    private AppRestService mRestService = AppRestController.getInstance().getAppRestService();

    public AddressActivityController(AddressActivity addressActivity) {
        super(addressActivity);
        getAddress();
    }

    protected String urlPdf;

    @OnClick(R.id.share_content)
    protected void shareContent() {
        AddressActivity activity = getActivity();
        if (activity != null) {
            String content = activity.tvAddress.getText().toString() + " " +
                    activity.tvPhone.getText().toString() + " " +
                    activity.tvEmail.getText().toString();
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, content);
            activity.startActivity(Intent.createChooser(sharingIntent, "Sharing Using"));

        }
    }

    private void getAddress() {
        mRestService.getAddress(new Callback<Address>() {
            @Override
            public void success(Address address, Response response) {
                onSuccess(address);
            }

            @Override
            public void failure(RetrofitError error) {
                onFail();
            }
        });
    }

    private void onSuccess(Address address) {
        if (address != null){
            urlPdf = address.getData().get(0).getPdfLink();
        }
    }

    private void onFail() {
        AddressActivity activity = getActivity();
        if (activity != null) {
            Snackbar snackbar = Snackbar.make(activity.containerAddress, "Terjadi kesalahan.", Snackbar.LENGTH_LONG)
                    .setAction("Ulangi", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getAddress();
                        }
                    });
            snackbar.show();
        }

    }

    @OnClick(R.id.iv_email)
    protected void sendEmail() {
        AddressActivity activity = getActivity();
        if (activity != null) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "concretetechnologyindonesia@gmail.com", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
            activity.startActivity(Intent.createChooser(emailIntent, "Send email..."));
        }
    }

    @OnClick(R.id.iv_phone)
    protected void callPhone() {
        AddressActivity activity = getActivity();
        if (activity != null) {
            String phone = "0318670202";
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
            activity.startActivity(intent);
        }
    }

    @OnClick(R.id.iv_download)
    protected void downloadPdf() {
        AddressActivity activity = getActivity();
        if (activity != null) {
            DownloadFileManager.getInstance().setContext(activity).showDownloadStatus(urlPdf, "Company_Profile.pdf");
        }
    }

    @OnClick(R.id.tv_download_company_profile)
    protected void viewPDF() {
        AddressActivity activity = getActivity();
        if (activity != null) {
            Intent intent = new Intent(activity, ViewCompro.class);
            intent.putExtra(ViewCompro.COMPRO, urlPdf);
            TransitionActivity.transitionRightToLeft(activity, intent);
        }
    }

    @OnClick(R.id.tv_cti_url)
    protected void gotoWeb() {
        AddressActivity activity = getActivity();
        if (activity != null) {
            String url = "http://concrete-technology-indonesia.com/";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            activity.startActivity(i);
        }
    }
}
