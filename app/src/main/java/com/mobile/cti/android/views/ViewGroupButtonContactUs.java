package com.mobile.cti.android.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.andexert.library.RippleView;
import com.mobile.cti.android.R;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by dirga on 29/07/16.
 */
public class ViewGroupButtonContactUs extends LinearLayout {
    @BindView(R.id.iv_left_icon_contact_us)
    ImageView ivLeftIconContactUs;
    @BindView(R.id.tv_title_contact_us)
    ISCTextView tvTitleContactUs;
    @BindView(R.id.item_container_contact_us)
    RippleView itemContainerContactUs;
    @BindView(R.id.rl_container_right_arrow)
    RelativeLayout rlContainerRightArrow;
    private OnButtonClickListener onButtonClickListener;
    private String text;

    public ViewGroupButtonContactUs(Context context) {
        super(context);
        init();
    }

    public ViewGroupButtonContactUs(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ViewGroupButtonContactUs(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View inflaterView = inflater.inflate(R.layout.view_group_button_contact_us, this, true);
        ButterKnife.bind(inflaterView, this);
        itemContainerContactUs.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonClickListener.onButtonClicked(text);
            }
        });
    }

    public void setTextTitle(String text) {
        this.text = text;
        tvTitleContactUs.setText(text);
    }

    public void setLeftIcon(int icon) {
        Glide.with(getContext()).load(icon).into(ivLeftIconContactUs);
    }

    public void setBackgroundRightArrow(int color){
        rlContainerRightArrow.setBackgroundColor(color);
    }

    public void setOnButtonClickListener(OnButtonClickListener onButtonClickListener) {
        this.onButtonClickListener = onButtonClickListener;
    }

    public void setBackgroundItem(int color){
        itemContainerContactUs.setBackgroundColor(color);
    }

    public interface OnButtonClickListener{
        void onButtonClicked(String text);
    }
}
