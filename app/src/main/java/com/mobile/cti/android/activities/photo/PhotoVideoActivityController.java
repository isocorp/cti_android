package com.mobile.cti.android.activities.photo;

import com.mobile.cti.android.base.BaseActivityController;

/**
 * Created by dirga on 26/07/16.
 */
public class PhotoVideoActivityController extends BaseActivityController<PhotoVideoActivity> {
    public PhotoVideoActivityController(PhotoVideoActivity genericPhotoVideoActivity) {
        super(genericPhotoVideoActivity);
    }
}
