package com.mobile.cti.android;

import android.app.Application;

import com.mobile.cti.android.R;
import com.mobile.cti.android.rest.AppRestController;
import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Dirga on 7/20/2016.
 */
public class CtiApplication extends Application {
    private AppRestController appRestService = AppRestController.getInstance();
    private Tracker mTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        appRestService.generateRest();
        Fabric.with(this, new Crashlytics());
        Stetho.initializeWithDefaults(this);
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.app_tracker);
        }
        return mTracker;
    }
}
