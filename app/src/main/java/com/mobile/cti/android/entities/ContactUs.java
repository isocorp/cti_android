package com.mobile.cti.android.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dirga on 8/18/2016.
 */
public class ContactUs extends BaseEntities {
    @SerializedName("data")
    @Expose
    private List<ContactUsImpl> data = new ArrayList<ContactUsImpl>();

    /**
     *
     * @return
     * The data
     */
    public List<ContactUsImpl> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<ContactUsImpl> data) {
        this.data = data;
    }
}
