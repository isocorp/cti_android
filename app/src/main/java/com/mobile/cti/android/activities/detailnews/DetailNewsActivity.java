package com.mobile.cti.android.activities.detailnews;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.mobile.cti.android.R;
import com.mobile.cti.android.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by Dirga on 8/16/2016.
 */
public class DetailNewsActivity extends BaseActivity {
    public static final String ARTICLE_ID = "article_id";
    @BindView(R.id.iv_detail_news)
    ImageView ivDetailNews;
    @BindView(R.id.pb_product_detail)
    ProgressBar pbProductDetail;
    @BindView(R.id.tv_detail_news_title)
    ISCTextView tvDetailNewsTitle;
    @BindView(R.id.tv_detail_news_date)
    ISCTextView tvDetailNewsDate;
    @BindView(R.id.web_view_detail_news)
    WebView webViewDetailNews;
    @BindView(R.id.container_news_detail)
    CoordinatorLayout containerNewsDetail;
    private Toolbar toolbar;
    protected String id;
    private DetailNewsActivityController controller;
    private ISCTextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new DetailNewsActivityController(this);
        if (getIntent().hasExtra(ARTICLE_ID)) {
            id = getIntent().getExtras().getString(ARTICLE_ID);
        }
        controller.getDetailArticles();
    }

    @Override
    public Toolbar getToolbar() {
        setContentView(R.layout.activity_detail_news);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar_detail_news);
        title = (ISCTextView) toolbar.findViewById(R.id.toolbar_title);
        title.setText("Detail News");
        return toolbar;
    }

    @Override
    public String toolbarTitle() {
        return null;
    }

    @Override
    public String screenName() {
        return null;
    }
}
