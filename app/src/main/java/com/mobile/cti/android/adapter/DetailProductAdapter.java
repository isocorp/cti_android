package com.mobile.cti.android.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.mobile.cti.android.R;
import com.mobile.cti.android.entities.ProductGetPDF;
import com.mobile.cti.android.entities.ProductImpl;

import java.util.ArrayList;
import java.util.List;

import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by Dirga on 9/7/2016.
 */
public class DetailProductAdapter extends RecyclerView.Adapter<DetailProductAdapter.Holder> {
    private List<ProductGetPDF> listPdf = new ArrayList<>();
    private Holder.OnItemClick onItemClick;

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_group_detail_product, parent, false);
        return new Holder(itemView);
    }

    public void setData(List<ProductGetPDF> data) {
        this.listPdf = data;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.setOnItemClick(onItemClick);
        holder.bind(listPdf.get(position));
    }

    @Override
    public int getItemCount() {
        return listPdf.size();
    }

    public void setOnItemClick(Holder.OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    public static class Holder extends RecyclerView.ViewHolder {
        ISCTextView title;
        OnItemClick onItemClick;
        LinearLayout containerDetailProduct;
        ImageButton ibPopUp;

        public Holder(View itemView) {
            super(itemView);
            if (itemView != null) {
                title = (ISCTextView) itemView.findViewById(R.id.tv_item_detail_product);
                containerDetailProduct = (LinearLayout) itemView.findViewById(R.id.container_product_detail);
                ibPopUp = (ImageButton) itemView.findViewById(R.id.ib_menu_item);
            }
        }

        public void bind(final ProductGetPDF product) {
            if (product != null) {
                title.setText(product.getTitle());
                containerDetailProduct.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClick.onItemClick(product.getPdfLink());
                    }
                });

                ibPopUp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClick.onPopUp(ibPopUp, product.getPdfLink());
                    }
                });
            }
        }

        public void setOnItemClick(OnItemClick onItemClick) {
            this.onItemClick = onItemClick;
        }

        public interface OnItemClick {
            void onItemClick(String urlPDF);

            void onPopUp(View view, String urlPdf);
        }

    }
}
