package com.mobile.cti.android.rest;

import com.mobile.cti.android.entities.Address;
import com.mobile.cti.android.entities.ContactUs;
import com.mobile.cti.android.entities.DetailNews;
import com.mobile.cti.android.entities.Galleries;
import com.mobile.cti.android.entities.News;
import com.mobile.cti.android.entities.Product;
import com.mobile.cti.android.entities.ProductDetail;
import com.mobile.cti.android.entities.Promo;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by dirga on 25/07/16.
 */
public interface AppRestService {
    @GET("/products")
    void getProduct(Callback<Product> callback);

    @GET("/promos")
    void getPromos(Callback<Promo> callback);

    @GET("/articles")
    void getNews(Callback<News> callback);

    @GET("/products/{id}")
    void getDetailProduct(@Path("id") String user, Callback<ProductDetail> callback);

    @GET("/galleries")
    void getGalleries(Callback<Galleries> callback);

    @GET("/galleries/video")
    void getGalleriesVideo(Callback<Galleries> callback);

    @GET("/galleries/image")
    void getGalleriesImage(Callback<Galleries> callback);

    @GET("/articles/{id}")
    void getDetailArticle(@Path("id") String id, Callback<DetailNews> callback);

    @GET("/contacts/sales")
    void getSales(Callback<ContactUs> callback);

    @GET("/contacts/cs")
    void getCustomerService(Callback<ContactUs> callback);

    @GET("/contacts/director")
    void getDirector(Callback<ContactUs> callback);

    @GET("/companies")
    void getAddress(Callback<Address> callback);

}
