package com.mobile.cti.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mobile.cti.android.R;
import com.mobile.cti.android.entities.ContactPerson;
import com.mobile.cti.android.views.ViewGroupButtonContactUs;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by dirga on 29/07/16.
 */

public class ContactUsItemAdapter extends RecyclerView.Adapter<ContactUsItemAdapter.ItemView> {
    private Context context;
    private List<ContactPerson> contactPersons = new ArrayList<>();
    private ItemView.OnItemClicked onItemClicked;

    public ContactUsItemAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<ContactPerson> contactPersons) {
        this.contactPersons = contactPersons;
        notifyDataSetChanged();
    }


    @Override
    public ItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_group_contact_us_item, parent, false);
        return new ItemView(itemView, context);
    }

    @Override
    public void onBindViewHolder(ItemView holder, int position) {
        holder.setOnItemClicked(onItemClicked);
        holder.bind(contactPersons.get(position), position);
    }

    @Override
    public int getItemCount() {
        return contactPersons.size();
    }

    public void setOnItemClicked(ItemView.OnItemClicked onItemClicked) {
        this.onItemClicked = onItemClicked;
    }

    public static class ItemView extends RecyclerView.ViewHolder {
        ViewGroupButtonContactUs btnPhone, btnWhatsApp, btnLine, btnBbm, btnEmail;
        ISCTextView name;
        ImageView photo;
        Context context;
        OnItemClicked onItemClicked;

        public ItemView(View itemView, Context context) {
            super(itemView);
            if (itemView != null) {
                this.context = context;
                name = (ISCTextView) itemView.findViewById(R.id.tv_contact_us_name);
                photo = (ImageView) itemView.findViewById(R.id.iv_header_contact_us);

                btnPhone = (ViewGroupButtonContactUs) itemView.findViewById(R.id.btn_phone);
                btnPhone.setLeftIcon(R.drawable.ic_headset);

                btnWhatsApp = (ViewGroupButtonContactUs) itemView.findViewById(R.id.btn_whatsapp);
                btnWhatsApp.setLeftIcon(R.drawable.ic_wa);
                btnWhatsApp.setBackgroundItem(context.getResources().getColor(R.color.btn_phone_contact_us_wa));
                btnWhatsApp.setBackgroundRightArrow(context.getResources().getColor(R.color.btn_phone_contact_us__wa_dark));

                btnLine = (ViewGroupButtonContactUs) itemView.findViewById(R.id.btn_line);
                btnLine.setLeftIcon(R.drawable.ic_line);
                btnLine.setBackgroundItem(context.getResources().getColor(R.color.btn_phone_contact_us_line));
                btnLine.setBackgroundRightArrow(context.getResources().getColor(R.color.btn_phone_contact_us__line_dark));

                btnEmail = (ViewGroupButtonContactUs) itemView.findViewById(R.id.btn_email);
                btnEmail.setLeftIcon(R.drawable.ic_email);
                btnEmail.setBackgroundItem(context.getResources().getColor(R.color.btn_phone_contact_us_email));
                btnEmail.setBackgroundRightArrow(context.getResources().getColor(R.color.btn_phone_contact_us__email_dark));

                btnBbm = (ViewGroupButtonContactUs) itemView.findViewById(R.id.btn_bbm);
                btnBbm.setLeftIcon(R.drawable.ic_bbm);
                btnBbm.setBackgroundItem(context.getResources().getColor(R.color.btn_phone_contact_us_bbm));
                btnBbm.setBackgroundRightArrow(context.getResources().getColor(R.color.btn_phone_contact_us__bbm_dark));
            }
        }

        public void bind(final ContactPerson contactPerson, int position) {
            if (contactPerson != null) {
                name.setText(contactPerson.getName());
                if (contactPerson.getPhone() != null) {
                    if (contactPerson.getPhone().length() <= 0) {
                        btnPhone.setVisibility(View.GONE);
                    } else {
                        btnPhone.setTextTitle(contactPerson.getPhone());
                        btnPhone.setOnButtonClickListener(new ViewGroupButtonContactUs.OnButtonClickListener() {
                            @Override
                            public void onButtonClicked(String text) {
                                onItemClicked.onPhoneClicked(contactPerson.getPhone());
                            }
                        });
                    }
                } else {
                    btnPhone.setVisibility(View.GONE);
                }

                if (contactPerson.getEmail() != null) {
                    if (contactPerson.getEmail().length() <= 0) {
                        btnEmail.setVisibility(View.GONE);
                    } else {
                        btnEmail.setTextTitle(contactPerson.getEmail());
                        btnEmail.setOnButtonClickListener(new ViewGroupButtonContactUs.OnButtonClickListener() {
                            @Override
                            public void onButtonClicked(String text) {
                                onItemClicked.onEmailClicked(contactPerson.getEmail());
                            }
                        });
                    }
                } else {
                    btnEmail.setVisibility(View.GONE);
                }

                if (contactPerson.getWhatsApp() != null) {
                    if (contactPerson.getWhatsApp().length() <= 0){
                        btnWhatsApp.setVisibility(View.GONE);
                    }else {
                        btnWhatsApp.setTextTitle(contactPerson.getWhatsApp());
                        btnWhatsApp.setOnButtonClickListener(new ViewGroupButtonContactUs.OnButtonClickListener() {
                            @Override
                            public void onButtonClicked(String text) {
                                onItemClicked.onWhatAppsclicked(contactPerson.getWhatsApp(), contactPerson.getName());
                            }
                        });
                    }
                } else {
                    btnWhatsApp.setVisibility(View.GONE);
                }

                if (contactPerson.getLine() != null) {
                    if (contactPerson.getLine().length() <= 0){
                        btnLine.setVisibility(View.GONE);
                    }else{
                        btnLine.setTextTitle(contactPerson.getLine());
                        btnLine.setOnButtonClickListener(new ViewGroupButtonContactUs.OnButtonClickListener() {
                            @Override
                            public void onButtonClicked(String text) {
                                onItemClicked.onLineClicked(contactPerson.getLine());
                            }
                        });
                    }

                } else {
                    btnLine.setVisibility(View.GONE);
                }

                if (contactPerson.getBbm() != null) {
                    if (contactPerson.getBbm().length() <= 0){
                        btnBbm.setVisibility(View.GONE);
                    }else {
                        btnBbm.setTextTitle(contactPerson.getBbm());
                        btnBbm.setOnButtonClickListener(new ViewGroupButtonContactUs.OnButtonClickListener() {
                            @Override
                            public void onButtonClicked(String text) {
                                onItemClicked.onBbmClicked(contactPerson.getBbm());
                            }
                        });
                    }

                } else {
                    btnBbm.setVisibility(View.GONE);
                }
                Glide.with(context).load(contactPerson.getImage()).dontAnimate().into(photo);
            }
        }

        public void setOnItemClicked(OnItemClicked onItemClicked) {
            this.onItemClicked = onItemClicked;
        }

        public interface OnItemClicked {
            void onWhatAppsclicked(String whatApps, String name);

            void onLineClicked(String line);

            void onBbmClicked(String bbm);

            void onEmailClicked(String email);

            void onPhoneClicked(String phone);
        }
    }
}
