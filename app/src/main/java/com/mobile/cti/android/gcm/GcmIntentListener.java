package com.mobile.cti.android.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.google.android.gms.gcm.GcmReceiver;
import com.mobile.cti.android.R;
import com.mobile.cti.android.activities.main.MainActivity;
import com.mobile.cti.android.activities.news.NewsActivity;
import com.mobile.cti.android.activities.photo.PhotoVideoActivity;
import com.mobile.cti.android.activities.product.ProductActivity;
import com.mobile.cti.android.activities.promo.PromoActivity;
import com.mobile.cti.android.activities.splash.SplashActivity;

import co.mobiwise.fastgcm.GCMListenerService;

/**
 * Created by Dirga on 7/22/2016.
 */
public class GcmIntentListener extends GCMListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);
        if (data != null) {
            String message = data.getString("message");
            String title = data.getString("title");
            String sub = data.getString("subtitle");
            String type = data.getString("type");
            sendNotification(title, message, sub, type);
        }
        //Here is called even app is not running.
        //create your notification here.
    }

    private void sendNotification(String title, String message, String subtitle, String type) {
        Intent intent = null;
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        if (!type.equals("_empty_")) {
            String typeLowerCase = type.toLowerCase();
            if (typeLowerCase.equals("product")) {
                intent = new Intent(this, ProductActivity.class);
            } else if ((typeLowerCase.equals("article"))) {
                intent = new Intent(this, NewsActivity.class);
            } else if (typeLowerCase.equals("gallery")) {
                intent = new Intent(this, PhotoVideoActivity.class);
            } else if (typeLowerCase.equals("promo")) {
                intent = new Intent(this, PromoActivity.class);
            }
        } else {
            intent = new Intent(this, SplashActivity.class);
        }
        stackBuilder.addNextIntent(intent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        //        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(message)
                .setCategory(subtitle)
                .setTicker(title)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0/* ID of notification */, notificationBuilder.build());
        GcmReceiver.completeWakefulIntent(intent);
    }
}
