package com.mobile.cti.android.activities.main;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.mobile.cti.android.R;
import com.mobile.cti.android.base.BaseActivity;
import com.mobile.cti.android.utils.GridSpacingItemDecoration;
import com.bumptech.glide.Glide;
import com.mobile.cti.android.utils.PreferenceManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list_main_menu)
    RecyclerView listMainMenu;
    @BindView(R.id.iv_main_menu_header)
    ImageView ivMainMenuHeader;
    @BindView(R.id.lr_contact_us)
    RippleView lrContactUs;
    private MainActivityController controller;
    int spanCount = 3; // 3 columns
    int spacing = 10; // 50px
    boolean includeEdge = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        controller = new MainActivityController(this);
        Glide.with(this).load(R.drawable.image_header_main).into(ivMainMenuHeader);
        GridLayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        listMainMenu.setLayoutManager(mLayoutManager);
        listMainMenu.setItemAnimator(new DefaultItemAnimator());
        listMainMenu.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge, this));
        listMainMenu.setAdapter(controller.adapter);
        lrContactUs.setOnRippleCompleteListener(controller);
//        copyID();
    }

    private void copyID(){
        String id = PreferenceManager.getGcmToken(this);
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Copy ID gcm", id);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this,"Copy to clipboard Id GCM",Toast.LENGTH_LONG).show();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public Toolbar getToolbar() {
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        return toolbar;
    }

    @Override
    public String toolbarTitle() {
        return null;
    }

    @Override
    public String screenName() {
        return "Main Screen";
    }
}
