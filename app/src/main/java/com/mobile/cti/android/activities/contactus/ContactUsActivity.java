package com.mobile.cti.android.activities.contactus;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.mobile.cti.android.R;
import com.mobile.cti.android.adapter.ContactUsPagerAdapter;
import com.mobile.cti.android.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by Dirga on 16/07/2016.
 */
public class ContactUsActivity extends BaseActivity {

    @BindView(R.id.tab_contact_us)
    TabLayout tabContactUs;
    @BindView(R.id.pager_contact_us)
    ViewPager pagerContactUs;
    private ISCTextView toolbarTitle;
    private Toolbar toolbar;
    private ContactUsPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pagerAdapter = new ContactUsPagerAdapter(getSupportFragmentManager());
        pagerContactUs.setAdapter(pagerAdapter);
        tabContactUs.setupWithViewPager(pagerContactUs);
    }

    @Override
    public Toolbar getToolbar() {
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        toolbar = (Toolbar)findViewById(R.id.toolbar_contact_us);
        toolbarTitle = (ISCTextView)toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Contact Us");
        return toolbar;
    }

    @Override
    public String toolbarTitle() {
        return null;
    }

    @Override
    public String screenName() {
        return "Contact Us";
    }

    private void init(){
        pagerAdapter = new ContactUsPagerAdapter(getSupportFragmentManager());
    }
}
