package com.mobile.cti.android.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dirga on 8/22/2016.
 */
public class Address extends BaseEntities {
    @SerializedName("data")
    @Expose
    private List<AddressImpl> data = new ArrayList<AddressImpl>();

    /**
     *
     * @return
     * The data
     */
    public List<AddressImpl> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<AddressImpl> data) {
        this.data = data;
    }
}
