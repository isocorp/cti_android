package com.mobile.cti.android.activities.photo;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.mobile.cti.android.R;
import com.mobile.cti.android.adapter.PhotoVideoPagerAdapter;
import com.mobile.cti.android.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by dirga on 26/07/16.
 */
public class PhotoVideoActivity extends BaseActivity {
    @BindView(R.id.tab_generic_promo)
    TabLayout tabGenericPromo;
    @BindView(R.id.vp_generic_promo)
    ViewPager vpGenericPromo;
    private Toolbar toolbar;
    private ISCTextView titlePage;
    private PhotoVideoPagerAdapter photoVideoPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        photoVideoPagerAdapter = new PhotoVideoPagerAdapter(getSupportFragmentManager());
        vpGenericPromo.setAdapter(photoVideoPagerAdapter);
        tabGenericPromo.setupWithViewPager(vpGenericPromo);
    }

    @Override
    public Toolbar getToolbar() {
        setContentView(R.layout.activity_photo_video);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar_generic_promo);
        titlePage = (ISCTextView) toolbar.findViewById(R.id.toolbar_title);
        titlePage.setText("Photo & Video");
        return toolbar;
    }

    @Override
    public String toolbarTitle() {
        return null;
    }

    @Override
    public String screenName() {
        return null;
    }
}
