package com.mobile.cti.android.activities.address;

import android.app.DownloadManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mobile.cti.android.R;
import com.mobile.cti.android.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by dirga on 06/08/16.
 */
public class AddressActivity extends BaseActivity implements OnMapReadyCallback {
    @BindView(R.id.tv_address)
    ISCTextView tvAddress;
    @BindView(R.id.tv_about_us)
    ISCTextView tvAboutUs;
    @BindView(R.id.tv_phone)
    ISCTextView tvPhone;
    @BindView(R.id.tv_email)
    ISCTextView tvEmail;
    @BindView(R.id.tv_download_company_profile)
    ISCTextView tvDownloadCompanyProfile;
    @BindView(R.id.share_content)
    FloatingActionButton shareContent;
    @BindView(R.id.iv_phone)
    ImageView ivPhone;
    @BindView(R.id.iv_email)
    ImageView ivEmail;
    @BindView(R.id.iv_download)
    ImageView ivDownload;
    @BindView(R.id.container_address)
    CoordinatorLayout containerAddress;
    private SupportMapFragment mapFragment;
    private static LatLng latLong;
    private Toolbar toolbar;
    private ISCTextView titlePage;
    private GoogleMap map;
    private AddressActivityController controller;
    protected DownloadManager downloadManagerAndroid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new AddressActivityController(this);
        downloadManagerAndroid = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_cti_office));
        mapFragment.getMapAsync(this);
        tvAddress.setText(Html.fromHtml(getResources().getString(R.string.address)));
        tvAboutUs.setText(Html.fromHtml(getResources().getString(R.string.about_us)));
        tvPhone.setText(Html.fromHtml(getResources().getString(R.string.phone_office)));
    }

    @Override
    public Toolbar getToolbar() {
        setContentView(R.layout.activity_address);
        ButterKnife.bind(this);
//        toolbar = (Toolbar) findViewById(R.id.toolbar_address_cti);
//        titlePage = (ISCTextView) toolbar.findViewById(R.id.toolbar_title);
//        titlePage.setText("Address");
        return null;
    }

    @Override
    public String toolbarTitle() {
        return "Address Page";
    }

    @Override
    public String screenName() {
        return "Address page";
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        setMaps(googleMap);
    }

    protected void setMaps(GoogleMap map) {
        if (map == null) {
            MapsInitializer.initialize(this);
        }
        latLong = new LatLng(-7.344863, 112.767279);
        Marker position = map.addMarker(new MarkerOptions()
                .position(latLong)
                .title("PT. Concrete Technology Indonesia")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

        // Move the camera instantly to hamburg with a zoom of 15.
//            CameraUpdate camera = CameraUpdateFactory.newLatLng(latLong);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLong, 15));

        // Zoom in, animating the camera.
        map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }

}
