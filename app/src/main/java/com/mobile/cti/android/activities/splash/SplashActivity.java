package com.mobile.cti.android.activities.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;

import com.mobile.cti.android.R;
import com.mobile.cti.android.activities.main.MainActivity;
import com.mobile.cti.android.base.BaseActivity;
import com.mobile.cti.android.utils.PreferenceManager;
import com.mobile.cti.android.utils.TransitionActivity;
import com.crashlytics.android.Crashlytics;

import java.io.IOException;

import co.mobiwise.fastgcm.GCMListener;
import co.mobiwise.fastgcm.GCMManager;

/**
 * Created by Dirga on 16/07/2016.
 */
public class SplashActivity extends BaseActivity implements GCMListener {
    private Handler handler;
    private SplashScreenController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new SplashScreenController(this);
        handler = new Handler();
        if (PreferenceManager.getGcmToken(SplashActivity.this) != null) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    TransitionActivity.transitionRightToLeft(SplashActivity.this, intent);
                    finish();
                }
            }, 3000);
        } else {
            GCMManager.getInstance(this).registerListener(this);
        }
    }

    @Override
    public Toolbar getToolbar() {
        setContentView(R.layout.activity_splash_screen);
        return null;
    }

    @Override
    public String toolbarTitle() {
        return null;
    }

    @Override
    public String screenName() {
        return "Splash Screen";
    }

    @Override
    public void onDeviceRegisted(String token) {
        if (token != null && token.length() > 0) {
            try {
                controller.subscribeTopics(token);
            } catch (IOException e) {
                e.printStackTrace();
            }
            PreferenceManager.setGcmToken(SplashActivity.this, token);
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            TransitionActivity.transitionRightToLeft(SplashActivity.this, intent);
            finish();
        } else {
            Exception exception = new Exception("Request token " + token);
            Crashlytics.getInstance().logException(exception);
        }
    }

    @Override
    public void onMessage(String s, Bundle bundle) {

    }

    @Override
    public void onPlayServiceError() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GCMManager.getInstance(this).unRegisterListener();
    }
}
