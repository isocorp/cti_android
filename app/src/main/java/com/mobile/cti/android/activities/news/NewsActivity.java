package com.mobile.cti.android.activities.news;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.widget.ProgressBar;

import com.mobile.cti.android.R;
import com.mobile.cti.android.base.BaseActivity;
import com.mobile.cti.android.views.GenericNewsLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by dirga on 27/07/16.
 */
public class NewsActivity extends BaseActivity {
    @BindView(R.id.lv_news)
    GenericNewsLayout lvNews;
    @BindView(R.id.pb_news)
    ProgressBar pbNews;
    @BindView(R.id.container_news)
    CoordinatorLayout containerNews;
    private Toolbar toolbar;
    private ISCTextView titlePage;
    private NewsActivityController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new NewsActivityController(this);
        lvNews.setOnItemNewsClickListener(controller);
//        lvNews.setData();
    }

    @Override
    public Toolbar getToolbar() {
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar_news);
        titlePage = (ISCTextView) toolbar.findViewById(R.id.toolbar_title);
        titlePage.setText("Articles");
        return toolbar;
    }

    @Override
    public String toolbarTitle() {
        return null;
    }

    @Override
    public String screenName() {
        return null;
    }
}
