package com.mobile.cti.android.activities.main;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;

import com.andexert.library.RippleView;
import com.mobile.cti.android.R;
import com.mobile.cti.android.activities.address.AddressActivity;
import com.mobile.cti.android.activities.contactus.ContactUsActivity;
import com.mobile.cti.android.activities.news.NewsActivity;
import com.mobile.cti.android.activities.photo.PhotoVideoActivity;
import com.mobile.cti.android.activities.product.ProductActivity;
import com.mobile.cti.android.activities.promo.PromoActivity;
import com.mobile.cti.android.adapter.MainAdapter;
import com.mobile.cti.android.base.BaseActivityController;
import com.mobile.cti.android.utils.TransitionActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.OnClick;

/**
 * Created by Dirga on 7/20/2016.
 */
public class MainActivityController extends BaseActivityController<MainActivity> implements MainAdapter.ItemView.OnClickItem, RippleView.OnRippleCompleteListener {
    protected MainAdapter adapter;
    private Handler handler;

    public MainActivityController(MainActivity mainActivity) {
        super(mainActivity);
        adapter = new MainAdapter(mainActivity);
        adapter.setOnClickItem(this);
        handler = new Handler();
    }

    @Override
    public void onItemClickListener(int position) {
        MainActivity activity = getActivity();
        if (activity != null){
            Intent intent;
            switch (position){
                case 0:
                    intent = new Intent(activity, PromoActivity.class);
                    TransitionActivity.transitionRightToLeft(activity,intent);
                    break;
                case 1:
                    intent = new Intent(activity, AddressActivity.class);
                    TransitionActivity.transitionRightToLeft(activity,intent);
                    break;
                case 2:
                    intent = new Intent(activity, NewsActivity.class);
                    TransitionActivity.transitionRightToLeft(activity,intent);
                    break;
                case 3:
                    intent = new Intent(activity, ProductActivity.class);
                    TransitionActivity.transitionRightToLeft(activity,intent);
                    break;
                case 4:
                    intent = new Intent(activity, PhotoVideoActivity.class);
                    TransitionActivity.transitionRightToLeft(activity,intent);
                    break;
                case 5:
                    callCalculator();
                    break;
            }
        }
    }

    @OnClick(R.id.lr_contact_us)
    protected void contactUs(){
//        final MainActivity activity = getActivity();
//        if (activity != null){
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                }
//            },500);
//        }
    }

    private void callCalculator(){
        MainActivity activity = getActivity();
        if (activity != null){
            ArrayList<HashMap<String,Object>> packages =new ArrayList<HashMap<String,Object>>();
            final PackageManager pm = activity.getPackageManager();
            List<PackageInfo> packs = pm.getInstalledPackages(0);
            for (PackageInfo pi : packs) {
                if( pi.packageName.toString().toLowerCase().contains("calcul")){
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map.put("appName", pi.applicationInfo.loadLabel(pm));
                    map.put("packageName", pi.packageName);
                    packages.add(map);
                }
            }

            if(packages.size()>=1){
                String packageName = (String) packages.get(0).get("packageName");
                Intent i = pm.getLaunchIntentForPackage(packageName);

                if (i != null)
                    activity.startActivity(i);
            }
        }
    }

    @Override
    public void onComplete(RippleView rippleView) {
        Intent intent = new Intent(activity, ContactUsActivity.class);
        TransitionActivity.transitionRightToLeft(activity,intent);
    }
}
