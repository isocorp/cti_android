package com.mobile.cti.android.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dirga on 9/7/2016.
 */
public class ProductGetPDF {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("file_original")
    @Expose
    private String fileOriginal;
    @SerializedName("pdf_link")
    @Expose
    private String pdfLink;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The fileOriginal
     */
    public String getFileOriginal() {
        return fileOriginal;
    }

    /**
     *
     * @param fileOriginal
     * The file_original
     */
    public void setFileOriginal(String fileOriginal) {
        this.fileOriginal = fileOriginal;
    }

    /**
     *
     * @return
     * The pdfLink
     */
    public String getPdfLink() {
        return pdfLink;
    }

    /**
     *
     * @param pdfLink
     * The pdf_link
     */
    public void setPdfLink(String pdfLink) {
        this.pdfLink = pdfLink;
    }
}
