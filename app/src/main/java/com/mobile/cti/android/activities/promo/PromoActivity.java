package com.mobile.cti.android.activities.promo;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.widget.ProgressBar;

import com.mobile.cti.android.R;
import com.mobile.cti.android.base.BaseActivity;
import com.mobile.cti.android.views.GenericNewsLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by dirga on 27/07/16.
 */
public class PromoActivity extends BaseActivity {

    @BindView(R.id.pb_promo)
    ProgressBar pbPromo;
    @BindView(R.id.container_promo)
    CoordinatorLayout containerPromo;
    private ISCTextView toolbarTitle;
    @BindView(R.id.lv_promo)
    GenericNewsLayout lvPromo;
    private Toolbar toolbar;
    private PromoActivityController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new PromoActivityController(this);
//        lvPromo.setData();
    }

    @Override
    public Toolbar getToolbar() {
        setContentView(R.layout.activity_promo);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar_promo);
        toolbarTitle = (ISCTextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Promo");
        return toolbar;
    }

    @Override
    public String toolbarTitle() {
        return null;
    }

    @Override
    public String screenName() {
        return null;
    }
}
