package com.mobile.cti.android.rest;

import retrofit.ErrorHandler;

import com.android.mobile.cti.cti_android_app.flavor.Flavors;
import com.facebook.stetho.okhttp.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.android.AndroidLog;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by dirga on 25/07/16.
 */
public class AppRestController implements ErrorHandler,RequestInterceptor {
    private static AppRestController instance = new AppRestController();
    private AppRestService appRestService;

    public void generateRest() {
        OkHttpClient client = new OkHttpClient();
        client.setReadTimeout(60, TimeUnit.SECONDS);
        client.setConnectTimeout(60,TimeUnit.SECONDS);
        client.networkInterceptors().add(new StethoInterceptor());
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        String baseUrl = Flavors.BASE_API_URL;
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(new AndroidLog("dirga"))
                .setEndpoint(baseUrl)
                .setRequestInterceptor(this)
                .setErrorHandler(this)
                .setConverter(new GsonConverter(gson))
                .setClient(new OkClient(client))
                .build();
        appRestService = restAdapter.create(AppRestService.class);
    }

    public AppRestService getAppRestService() {
        return appRestService;
    }


    public static AppRestController getInstance(){
        return instance;
    }

    @Override
    public Throwable handleError(RetrofitError error){
        return error;
    }

    @Override
    public void intercept(RequestFacade requestFacade) {

    }
}
