package com.mobile.cti.android.activities.detilproduct;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.mobile.cti.android.R;
import com.mobile.cti.android.activities.viewcompro.ViewCompro;
import com.mobile.cti.android.adapter.DetailProductAdapter;
import com.mobile.cti.android.base.BaseActivityController;
import com.mobile.cti.android.entities.ProductDetail;
import com.mobile.cti.android.rest.AppRestController;
import com.mobile.cti.android.rest.AppRestService;
import com.mobile.cti.android.utils.DownloadFileManager;
import com.mobile.cti.android.utils.TransitionActivity;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Dirga on 8/12/2016.
 */
public class DetailProductActivityController extends BaseActivityController<DetailProductActivity> implements DetailProductAdapter.Holder.OnItemClick {
    private AppRestService mRestService = AppRestController.getInstance().getAppRestService();
    protected ProductDetail productDetail;

    public DetailProductActivityController(DetailProductActivity detailProductActivity) {
        super(detailProductActivity);
    }

    protected void getDetailProduct() {
        DetailProductActivity activity = getActivity();
        if (activity != null) {
            mRestService.getDetailProduct(activity.idProduct, new Callback<ProductDetail>() {
                @Override
                public void success(ProductDetail productDetail, Response response) {
                    onSuccess(productDetail);
                }

                @Override
                public void failure(RetrofitError error) {
                    onFail(error);
                }
            });
        }
    }

    private void onSuccess(ProductDetail productDetail) {
        DetailProductActivity activity = getActivity();
        if (activity != null) {
            try {
                this.productDetail = productDetail;
                activity.pbProductDetail.setVisibility(View.GONE);
//                Glide.with(activity).load(productDetail.getBaseUrl() + "/300/" + productDetail.getFileOriginal()).dontAnimate().into(activity.ivDetilProduct);
            } catch (Exception e) {
                Crashlytics.logException(e);
                Toast.makeText(activity, "Terjadi kesalahan", Toast.LENGTH_LONG).show();
            }

        }
    }

    private void onFail(RetrofitError error) {
        final DetailProductActivity activity = getActivity();
        if (activity != null) {
            activity.getPbProductDetail().setVisibility(View.GONE);
            Snackbar snackbar = Snackbar.make(activity.getContainerProductDetail(), "Terjadi kesalahan.", Snackbar.LENGTH_LONG)
                    .setAction("Ulang", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            activity.getPbProductDetail().setVisibility(View.VISIBLE);
                            getDetailProduct();
                        }
                    });
            snackbar.show();
        }
    }

//    protected void downloadProduct() {
//        String[] download = new String[]{"Download Image","Download PDF"};
//        final DetailProductActivity activity = getActivity();
//        if (activity != null) {
//            new MaterialDialog.Builder(activity)
//                    .title("Download")
//                    .items(download)
//                    .itemsCallback(new MaterialDialog.ListCallback() {
//                        @Override
//                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
//                            switch (which){
//                                case 0:
//                                    DownloadFileManager.getInstance().setContext(activity).showDownloadStatus(productDetail.getFileName(), productDetail.getFileOriginal());
//                                    break;
//                                case 1:
//                                    DownloadFileManager.getInstance().setContext(activity).showDownloadStatus(productDetail.getPdfLink(), productDetail.getTitle()+".pdf");
//                                    break;
//                            }
//                        }
//                    })
//                    .show();
//        }
//    }

    @Override
    public void onItemClick(String urlPDF) {
        DetailProductActivity activity = getActivity();
        if (activity != null) {
            Intent intent = new Intent(activity, ViewCompro.class);
            intent.putExtra(ViewCompro.COMPRO, urlPDF);
            TransitionActivity.transitionRightToLeft(activity, intent);
        }
    }

    @Override
    public void onPopUp(View view, final String urlPdf) {
        final DetailProductActivity activity = getActivity();
        if (activity != null) {
            PopupMenu popupMenu = new PopupMenu(activity, view);
            popupMenu.getMenuInflater().inflate(R.menu.pop_up_product, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.share_content:
                            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                            sharingIntent.setType("text/plain");
                            sharingIntent.putExtra(Intent.EXTRA_TEXT, urlPdf);
                            activity.startActivity(Intent.createChooser(sharingIntent, "Sharing Using"));
                            break;
                        case R.id.download_pdf:
                            String fileName = urlPdf.substring(urlPdf.lastIndexOf('/')+1, urlPdf.length());
                            DownloadFileManager.getInstance().setContext(activity).showDownloadStatus(urlPdf, fileName);
                            break;
                    }
//                    Toast.makeText(activity,"You Clicked : " + item.getItemId(),Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            popupMenu.show();
        }
    }
}
