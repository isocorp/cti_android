package com.mobile.cti.android.activities.splash;

import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.mobile.cti.android.base.BaseActivityController;
import com.mobile.cti.android.utils.Constants;

import java.io.IOException;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Dirga on 7/22/2016.
 */
public class SplashScreenController extends BaseActivityController<SplashActivity> {
    public SplashScreenController(SplashActivity splashActivity) {
        super(splashActivity);
    }

    protected void subscribeTopics(final String token) throws IOException {
        final SplashActivity activity = getActivity();
        if (activity != null) {
            Observable.create(new Observable.OnSubscribe<String>() {
                @Override
                public void call(Subscriber subscriber) {
//                subscriber.onNext(longRunningOperation());
//                subscriber.onCompleted();
                    GcmPubSub pubSub = GcmPubSub.getInstance(activity);
                    for (String topic : Constants.TOPICS) {
                        try {
                            pubSub.subscribe(token, "/topics/" + topic, null);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    subscriber.onCompleted();
                }
            })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<String>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e("error",e.toString());
                        }

                        @Override
                        public void onNext(String s) {
                            Log.d("success",s);
                        }
                    });
        }

//
//
//        if (activity != null) {
//            GcmPubSub pubSub = GcmPubSub.getInstance(activity);
//            for (String topic : Constants.TOPICS) {
//                pubSub.subscribe(token, "/topics/" + topic, null);
//            }
//        }
    }
}
