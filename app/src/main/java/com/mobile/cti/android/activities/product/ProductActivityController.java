package com.mobile.cti.android.activities.product;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.mobile.cti.android.activities.detilproduct.DetailProductActivity;
import com.mobile.cti.android.base.BaseActivityController;
import com.mobile.cti.android.entities.GenericGridData;
import com.mobile.cti.android.entities.Product;
import com.mobile.cti.android.entities.ProductImpl;
import com.mobile.cti.android.rest.AppRestController;
import com.mobile.cti.android.rest.AppRestService;
import com.mobile.cti.android.utils.TransitionActivity;
import com.mobile.cti.android.views.GenericGridLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dirga on 26/07/16.
 */
public class ProductActivityController extends BaseActivityController<ProductActivity> implements GenericGridLayout.OnItemClickListener {
    private AppRestService mRestService = AppRestController.getInstance().getAppRestService();
    private List<GenericGridData> data = new ArrayList<>();
    private List<ProductImpl> listPDF = new ArrayList<>();
    public ProductActivityController(ProductActivity productActivity) {
        super(productActivity);
        productActivity.gridProduct.setOnItemClickListener(this);
        getProduct();
    }

    private void getProduct(){
        mRestService.getProduct(new Callback<Product>() {
            @Override
            public void success(Product product, Response response) {
                onSuccess(product);
            }

            @Override
            public void failure(RetrofitError error) {
                onFail(error);
            }
        });
    }

    private void onSuccess(Product product) {
        ProductActivity activity = getActivity();
        if (activity != null) {
            if (product != null) {
                activity.pbProduct.setVisibility(View.GONE);
                listPDF = product.getData();
                for (int i = 0; i < product.getData().size(); i++){
                    GenericGridData listData = new GenericGridData();
                    listData.setId(String.valueOf(product.getData().get(i).getId()));
//                    listData.setTitle(product.getData().get(i).getTitle());
                    listData.setImage(product.getData().get(i).getFileName());
                    listData.setViewCount(product.getData().get(i).getViewCount());
                    listData.setType("image");
                    listData.setBaseUrl(product.getData().get(i).getBaseUrl());
                    listData.setOriginalFileName(product.getData().get(i).getFileOriginal());
                    listData.setUrlPDF(product.getData().get(i).getPdfLink());
                    data.add(listData);
                }
            }
            activity.gridProduct.setDataGrid(data);
        }

    }

    private void onFail(RetrofitError error) {
        final ProductActivity activity = getActivity();
        if (activity != null){
            activity.pbProduct.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(activity.productContainer, "Sepertinya terjadi kesalahan.", Snackbar.LENGTH_LONG)
                    .setAction("Ulangi", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            activity.pbProduct.setVisibility(View.VISIBLE);
                            getProduct();
                        }
                    });

            snackbar.show();
        }

    }

    @Override
    public void onItemClicked(String id, String type, String url, int position) {
        ProductActivity activity = getActivity();
        if (activity != null){
            Intent intent = new Intent(activity, DetailProductActivity.class);
            intent.putExtra(DetailProductActivity.ID_PRODUCT,id);
            EventBus.getDefault().postSticky(listPDF.get(position).getData());
            TransitionActivity.transitionRightToLeft(activity, intent);
        }
    }

}
