package com.mobile.cti.android.activities.generaldetail;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.mobile.cti.android.R;
import com.mobile.cti.android.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by dirga on 31/08/16.
 */
public abstract class GeneralDetailActivity extends BaseActivity {
    private Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    ISCTextView toolbarTitle;
    @BindView(R.id.container_product_detail)
    CoordinatorLayout containerProductDetail;
    @BindView(R.id.pb_product_detail)
    ProgressBar pbProductDetail;

    @Override
    public Toolbar getToolbar() {
        setContentView(R.layout.activity_detil_product);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar_product_detil);
        toolbarTitle = (ISCTextView) toolbarTitle.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("");
        return toolbar;
    }

    public ProgressBar getPbProductDetail() {
        return pbProductDetail;
    }

//    public ImageView getIvDetilProduct() {
//        return ivDetilProduct;
//    }
//
//    public FloatingActionButton getFabDownload() {
//        return fabDownload;
//    }

    public CoordinatorLayout getContainerProductDetail() {
        return containerProductDetail;
    }
}
