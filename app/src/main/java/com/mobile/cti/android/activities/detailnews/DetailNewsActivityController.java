package com.mobile.cti.android.activities.detailnews;

import android.support.design.widget.Snackbar;
import android.view.View;

import com.mobile.cti.android.base.BaseActivityController;
import com.mobile.cti.android.entities.DetailNews;
import com.mobile.cti.android.rest.AppRestController;
import com.mobile.cti.android.rest.AppRestService;
import com.bumptech.glide.Glide;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Dirga on 8/16/2016.
 */
public class DetailNewsActivityController extends BaseActivityController<DetailNewsActivity> {
    private AppRestService mRestService = AppRestController.getInstance().getAppRestService();

    public DetailNewsActivityController(DetailNewsActivity detailNewsActivity) {
        super(detailNewsActivity);
    }

    protected void getDetailArticles() {
        final DetailNewsActivity activity = getActivity();
        if (activity != null) {
            mRestService.getDetailArticle(activity.id, new Callback<DetailNews>() {
                @Override
                public void success(DetailNews detailNews, Response response) {
                    activity.pbProductDetail.setVisibility(View.GONE);
                    onSuccess(detailNews);
                }

                @Override
                public void failure(RetrofitError error) {
                    onFail(error);
                }
            });
        }
    }

    private void onSuccess(DetailNews detailNews) {
        DetailNewsActivity activity = getActivity();
        if (activity != null) {
            if (detailNews != null) {
                Glide.with(activity).load(detailNews.getFileName()).dontAnimate().into(activity.ivDetailNews);
                activity.tvDetailNewsTitle.setText(detailNews.getTitle());
                activity.tvDetailNewsDate.setText(detailNews.getUpdatedAt());
                String htmlContent = "<html <head>\n" +
                        "<title></title>\n" +
                        "</head>\n" +
                        "<body " + detailNews.getBody() + "</body></html>";
                activity.webViewDetailNews.loadData(htmlContent, "text/html", "UTF-8");
            }
        }
    }

    private void onFail(RetrofitError error) {
        final DetailNewsActivity activity = getActivity();
        if (activity != null) {
            activity.pbProductDetail.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar.make(activity.containerNewsDetail, "Terjadi kesalahan", Snackbar.LENGTH_LONG)
                    .setAction("Ulangi", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            activity.pbProductDetail.setVisibility(View.VISIBLE);
                            getDetailArticles();
                        }
                    });
        }

    }
}
