package com.mobile.cti.android.utils;

/**
 * Created by Dirga on 03/03/2016.
 */
public class Constants {
    //    PREFERENCE MANAGER
    public static String PREFERENCE_TOKEN = "preference-token";
    public static String PREFERENCE_ID = "preference-id";
    public static String PREFERENCE_TOKEN_GCM = "preference-id-gcm";
    public static String PREFERENCE_END_DATE = "preference-end-date";
    public static String FACEBOOK_ID = "facebook_id";

    public static String CONTACT_POSITION = "position_contact";

    public static String PACKAGE_NAME_WHATSAPP = "com.whatsapp";
    public static String PACKAGE_NAME_LINE = "jp.naver.line.android";
    public static String PACKAGE_NAME_BBM = "com.bbm";
    public static final String[] TOPICS = {"global", "android"};

}
