package com.mobile.cti.android.activities.news;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.mobile.cti.android.activities.detailnews.DetailNewsActivity;
import com.mobile.cti.android.base.BaseActivityController;
import com.mobile.cti.android.entities.GenericNews;
import com.mobile.cti.android.entities.News;
import com.mobile.cti.android.rest.AppRestController;
import com.mobile.cti.android.rest.AppRestService;
import com.mobile.cti.android.utils.TransitionActivity;
import com.mobile.cti.android.views.GenericNewsLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dirga on 27/07/16.
 */
public class NewsActivityController extends BaseActivityController<NewsActivity> implements GenericNewsLayout.onItemNewsClickListener {
    private AppRestService mRestService = AppRestController.getInstance().getAppRestService();
    private List<GenericNews> dataNews = new ArrayList<>();

    public NewsActivityController(NewsActivity newsActivity) {
        super(newsActivity);
        getNews();
    }

    private void getNews() {
        mRestService.getNews(new Callback<News>() {
            @Override
            public void success(News news, Response response) {
                onSuccess(news);
            }

            @Override
            public void failure(RetrofitError error) {
                onFail(error);
            }
        });
    }

    private void onSuccess(News news) {
        final NewsActivity activity = getActivity();
        if (activity != null) {
            activity.pbNews.setVisibility(View.GONE);
            for (int i = 0; i < news.getData().size(); i++) {
                GenericNews article = new GenericNews();
                article.setId(String.valueOf(news.getData().get(i).getId()));
                article.setImage(news.getData().get(i).getFileName());
                article.setTitle(news.getData().get(i).getTitle());
                article.setDate(news.getData().get(i).getCreatedAt());
                dataNews.add(article);
            }
            activity.lvNews.setData(dataNews);
        }
    }

    private void onFail(RetrofitError error) {
        final NewsActivity activity = getActivity();
        Crashlytics.getInstance().logException(error);
        if (activity != null) {
            activity.pbNews.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar.make(activity.containerNews, "Terjadi kesalahan.", Snackbar.LENGTH_LONG)
                    .setAction("Ulang", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            activity.pbNews.setVisibility(View.VISIBLE);
                            getNews();
                        }
                    });
            snackbar.show();
        }

    }

    @Override
    public void onItemClicked(String id) {
        NewsActivity activity = getActivity();
        if (activity != null) {
            Intent intent = new Intent(activity, DetailNewsActivity.class);
            intent.putExtra(DetailNewsActivity.ARTICLE_ID, id);
            TransitionActivity.transitionRightToLeft(activity, intent);
        }

    }
}
