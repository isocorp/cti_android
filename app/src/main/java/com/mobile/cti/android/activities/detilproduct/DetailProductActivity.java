package com.mobile.cti.android.activities.detilproduct;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.mobile.cti.android.R;
import com.mobile.cti.android.activities.generaldetail.GeneralDetailActivity;
import com.mobile.cti.android.adapter.DetailProductAdapter;
import com.mobile.cti.android.entities.ProductGetPDF;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by Dirga on 8/12/2016.
 */
public class DetailProductActivity extends GeneralDetailActivity {
    public static final String ID_PRODUCT = "id_product";
    public static final String ID_GALLERY = "id_gallery";
    @BindView(R.id.toolbar_title)
    ISCTextView toolbarTitle;
    @BindView(R.id.container_product_detail)
    CoordinatorLayout containerProductDetail;
    @BindView(R.id.pb_product_detail)
    ProgressBar pbProductDetail;
    @BindView(R.id.lv_detail_product)
    RecyclerView lvDetailProduct;
    private Toolbar toolbar;
    protected String idProduct;
    protected String urlPdf;
    private DetailProductActivityController controller;
    protected DetailProductAdapter adapter;

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new DetailProductActivityController(this);
        adapter = new DetailProductAdapter();
        adapter.setOnItemClick(controller);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        lvDetailProduct.setLayoutManager(mLayoutManager);
        lvDetailProduct.setItemAnimator(new DefaultItemAnimator());
        lvDetailProduct.setAdapter(adapter);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.share_menu_toolbar, menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        String content = "";
//        switch (item.getItemId()) {
//            case R.id.share_menu_toolbar:
//                if (getIntent().hasExtra(ID_PRODUCT)) {
//                    content = controller.productDetail.getFileName();
//                } else if (getIntent().hasExtra(ID_GALLERY)) {
//                    content = getIntent().getExtras().getString(ID_GALLERY);
//                }
//                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                sharingIntent.putExtra(Intent.EXTRA_TEXT, content);
//                startActivity(Intent.createChooser(sharingIntent, "Sharing Using"));
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
//    }


    @Override
    public String toolbarTitle() {
        return null;
    }

    @Override
    public String screenName() {
        return "";
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void getListPDF(List<ProductGetPDF> listPDF){
        pbProductDetail.setVisibility(View.GONE);
        adapter.setData(listPDF);
    }
}
