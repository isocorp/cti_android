package com.mobile.cti.android.activities.promo;

import android.support.design.widget.Snackbar;
import android.view.View;

import com.mobile.cti.android.base.BaseActivityController;
import com.mobile.cti.android.entities.GenericNews;
import com.mobile.cti.android.entities.Promo;
import com.mobile.cti.android.rest.AppRestController;
import com.mobile.cti.android.rest.AppRestService;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dirga on 27/07/16.
 */
public class PromoActivityController extends BaseActivityController<PromoActivity> {
    private AppRestService mRestService = AppRestController.getInstance().getAppRestService();
    private List<GenericNews> dataNews = new ArrayList<>();

    public PromoActivityController(PromoActivity promoActivity) {
        super(promoActivity);
        getPromo();
    }

    private void getPromo() {
        mRestService.getPromos(new Callback<Promo>() {
            @Override
            public void success(Promo promo, Response response) {
                onSuccess(promo);
            }

            @Override
            public void failure(RetrofitError error) {
                onFail(error);
            }
        });
    }

    private void onSuccess(Promo promo) {
        PromoActivity activity = getActivity();
        if (activity != null) {
            activity.pbPromo.setVisibility(View.GONE);
            for (int i = 0; i < promo.getData().size(); i++) {
                GenericNews news = new GenericNews();
                news.setId(String.valueOf(promo.getData().get(i).getId()));
                news.setImage(promo.getData().get(i).getFileName());
                news.setTitle(promo.getData().get(i).getTitle());
                news.setDate(promo.getData().get(i).getCreatedAt());
                dataNews.add(news);
            }
            activity.lvPromo.setData(dataNews);
        }
    }

    private void onFail(RetrofitError error) {
        final PromoActivity activity = getActivity();
        if (activity != null) {
            activity.pbPromo.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar.make(activity.containerPromo, "Terjadi kesalahan", Snackbar.LENGTH_LONG)
                    .setAction("Ulangi", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            activity.pbPromo.setVisibility(View.VISIBLE);
                            getPromo();
                        }
                    });
            snackbar.show();
        }

    }
}
