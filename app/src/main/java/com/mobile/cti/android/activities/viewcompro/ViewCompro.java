package com.mobile.cti.android.activities.viewcompro;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mobile.cti.android.R;
import com.mobile.cti.android.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Dirga on 8/12/2016.
 */
public class ViewCompro extends BaseActivity {
    public static final String COMPRO = "compro";
    @BindView(R.id.wv_compro)
    WebView wvCompro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String url;
        if (getIntent().hasExtra(COMPRO)){
            url = getIntent().getExtras().getString(COMPRO);
            wvCompro.getSettings().setJavaScriptEnabled(true);
            wvCompro.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView webView, String urlNewString) {
                    return true;
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                }

                @Override
                public void onPageFinished(WebView view, String url) {

                }
            });
            wvCompro.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + url);
        }
    }

    @Override
    public Toolbar getToolbar() {
        setContentView(R.layout.activity_view_compro);
        ButterKnife.bind(this);
        return null;
    }

    @Override
    public String toolbarTitle() {
        return null;
    }

    @Override
    public String screenName() {
        return null;
    }
}
