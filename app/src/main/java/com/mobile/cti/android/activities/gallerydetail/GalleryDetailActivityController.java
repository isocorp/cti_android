package com.mobile.cti.android.activities.gallerydetail;

import com.mobile.cti.android.R;
import com.mobile.cti.android.activities.detilproduct.DetailProductActivity;
import com.mobile.cti.android.base.BaseActivityController;
import com.mobile.cti.android.utils.DownloadFileManager;

import butterknife.OnClick;

/**
 * Created by Dirga on 9/7/2016.
 */
public class GalleryDetailActivityController extends BaseActivityController<GalleryDetailActivity> {
    public GalleryDetailActivityController(GalleryDetailActivity galleryDetailActivity) {
        super(galleryDetailActivity);
    }

    @OnClick(R.id.fab_download_gallery)
    protected void downloadProduct() {
        GalleryDetailActivity activity = getActivity();
        if (activity != null) {
            DownloadFileManager.getInstance().setContext(activity).showDownloadStatus(activity.urlGallery, "gallery_photo");
        }
    }
}
