package com.mobile.cti.android.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by dirga on 25/07/16.
 */
public class FragmentContactUsAdapter extends RecyclerView.Adapter<FragmentContactUsAdapter.ItemView> {

    @Override
    public ItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ItemView holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class ItemView extends RecyclerView.ViewHolder {
        public ItemView(View itemView) {
            super(itemView);
        }
    }
}
