package com.mobile.cti.android.activities.gallerydetail;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.mobile.cti.android.R;
import com.mobile.cti.android.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.isocorp.isccore.views.ISCTextView;

/**
 * Created by Dirga on 9/7/2016.
 */
public class GalleryDetailActivity extends BaseActivity {
    public static final String ID_GALLERY = "id_gallery";
    protected String urlGallery;
    @BindView(R.id.toolbar_title)
    ISCTextView toolbarTitle;
    @BindView(R.id.pb_product_detail_gallery)
    ProgressBar pbProductDetailGallery;
    @BindView(R.id.fab_download_gallery)
    FloatingActionButton fabDownloadGallery;
    @BindView(R.id.container_product_detail)
    CoordinatorLayout containerProductDetail;
    @BindView(R.id.iv_detil_gallery)
    ImageView ivDetilGallery;
    private GalleryDetailActivityController controller;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new GalleryDetailActivityController(this);
        if (getIntent().hasExtra(ID_GALLERY)) {
            urlGallery = getIntent().getExtras().getString(ID_GALLERY);
            Glide.with(this).load(urlGallery).dontAnimate().into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    pbProductDetailGallery.setVisibility(View.GONE);
                    ivDetilGallery.setImageDrawable(resource);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.share_menu_toolbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String content = "";
        switch (item.getItemId()) {
            case R.id.share_menu_toolbar:
                if (getIntent().hasExtra(ID_GALLERY)) {
                    content = getIntent().getExtras().getString(ID_GALLERY);
                }
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, content);
                startActivity(Intent.createChooser(sharingIntent, "Sharing Using"));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public Toolbar getToolbar() {
        setContentView(R.layout.activity_gallery_detail);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar_gallery_detail);
        toolbarTitle.setText("");
        return toolbar;
    }

    @Override
    public String toolbarTitle() {
        return null;
    }

    @Override
    public String screenName() {
        return "Detail gallery";
    }
}
