package com.mobile.cti.android.activities.contactus;

import com.mobile.cti.android.base.BaseActivityController;

/**
 * Created by dirga on 25/07/16.
 */
public class ContactUsController extends BaseActivityController<ContactUsActivity> {
    public ContactUsController(ContactUsActivity contactUsActivity) {
        super(contactUsActivity);
    }
}
