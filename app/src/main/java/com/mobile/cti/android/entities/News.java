package com.mobile.cti.android.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dirga on 8/11/2016.
 */
public class News extends BaseEntities {
    @SerializedName("data")
    @Expose
    private List<NewsImpl> data = new ArrayList<NewsImpl>();

    /**
     *
     * @return
     * The data
     */
    public List<NewsImpl> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<NewsImpl> data) {
        this.data = data;
    }
}
