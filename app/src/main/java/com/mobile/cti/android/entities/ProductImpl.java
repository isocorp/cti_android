package com.mobile.cti.android.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dirga on 8/11/2016.
 */
public class ProductImpl {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("pdf_link")
    @Expose
    private String pdfLink;
    @SerializedName("file_name")
    @Expose
    private String fileName;
    @SerializedName("file_type")
    @Expose
    private String fileType;
    @SerializedName("file_original")
    @Expose
    private String fileOriginal;
    @SerializedName("base_url")
    @Expose
    private String baseUrl;
    @SerializedName("view_count")
    @Expose
    private String viewCount;
    @SerializedName("is_publish")
    @Expose
    private Boolean isPublish;
    @SerializedName("published_by")
    @Expose
    private String publishedBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("data")
    @Expose
    private List<ProductGetPDF> data = new ArrayList<ProductGetPDF>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The pdfLink
     */
    public String getPdfLink() {
        return pdfLink;
    }

    /**
     *
     * @param pdfLink
     * The pdf_link
     */
    public void setPdfLink(String pdfLink) {
        this.pdfLink = pdfLink;
    }

    /**
     *
     * @return
     * The fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     *
     * @param fileName
     * The file_name
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     *
     * @return
     * The fileType
     */
    public String getFileType() {
        return fileType;
    }

    /**
     *
     * @param fileType
     * The file_type
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     *
     * @return
     * The fileOriginal
     */
    public String getFileOriginal() {
        return fileOriginal;
    }

    /**
     *
     * @param fileOriginal
     * The file_original
     */
    public void setFileOriginal(String fileOriginal) {
        this.fileOriginal = fileOriginal;
    }

    /**
     *
     * @return
     * The baseUrl
     */
    public String getBaseUrl() {
        return baseUrl;
    }

    /**
     *
     * @param baseUrl
     * The base_url
     */
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     *
     * @return
     * The viewCount
     */
    public String getViewCount() {
        return viewCount;
    }

    /**
     *
     * @param viewCount
     * The view_count
     */
    public void setViewCount(String viewCount) {
        if (viewCount == null){
            viewCount = "0";
        }
        this.viewCount = viewCount;
    }

    /**
     *
     * @return
     * The isPublish
     */
    public Boolean getIsPublish() {
        return isPublish;
    }

    /**
     *
     * @param isPublish
     * The is_publish
     */
    public void setIsPublish(Boolean isPublish) {
        this.isPublish = isPublish;
    }

    /**
     *
     * @return
     * The publishedBy
     */
    public String getPublishedBy() {
        return publishedBy;
    }

    /**
     *
     * @param publishedBy
     * The published_by
     */
    public void setPublishedBy(String publishedBy) {
        this.publishedBy = publishedBy;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The data
     */
    public List<ProductGetPDF> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<ProductGetPDF> data) {
        this.data = data;
    }
}
