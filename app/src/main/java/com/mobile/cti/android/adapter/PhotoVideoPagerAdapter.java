package com.mobile.cti.android.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mobile.cti.android.fragments.FragmentPhotoVideo;

/**
 * Created by dirga on 26/07/16.
 */
public class PhotoVideoPagerAdapter extends FragmentStatePagerAdapter {
    private String[] titlePage = new String[3];

    public PhotoVideoPagerAdapter(FragmentManager fm) {
        super(fm);
        initTitle();
    }

    private void initTitle(){
        titlePage[0]="ALL";
        titlePage[1]="PHOTOS";
        titlePage[2]="VIDEOS";
    }

    @Override
    public Fragment getItem(int position) {
        return FragmentPhotoVideo.newInstance(position);
    }

    @Override
    public int getCount() {
        return titlePage.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titlePage[position];
    }
}
