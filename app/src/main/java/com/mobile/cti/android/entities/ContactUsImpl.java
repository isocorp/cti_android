package com.mobile.cti.android.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dirga on 8/18/2016.
 */
public class ContactUsImpl {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("division")
    @Expose
    private String division;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("file_type")
    @Expose
    private String fileType;
    @SerializedName("file_original")
    @Expose
    private String fileOriginal;
    @SerializedName("base_url")
    @Expose
    private String baseUrl;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("bbm")
    @Expose
    private String bbm;
    @SerializedName("whatsapp")
    @Expose
    private String whatsapp;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("line")
    @Expose
    private String line;
    @SerializedName("smartfren")
    @Expose
    private String smartfren;
    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The division
     */
    public String getDivision() {
        return division;
    }

    /**
     *
     * @param division
     * The division
     */
    public void setDivision(String division) {
        this.division = division;
    }

    /**
     *
     * @return
     * The position
     */
    public String getPosition() {
        return position;
    }

    /**
     *
     * @param position
     * The position
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     *
     * @return
     * The fileType
     */
    public String getFileType() {
        return fileType;
    }

    /**
     *
     * @param fileType
     * The file_type
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     *
     * @return
     * The fileOriginal
     */
    public String getFileOriginal() {
        return fileOriginal;
    }

    /**
     *
     * @param fileOriginal
     * The file_original
     */
    public void setFileOriginal(String fileOriginal) {
        this.fileOriginal = fileOriginal;
    }

    /**
     *
     * @return
     * The baseUrl
     */
    public String getBaseUrl() {
        return baseUrl;
    }

    /**
     *
     * @param baseUrl
     * The base_url
     */
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The bbm
     */
    public String getBbm() {
        return bbm;
    }

    /**
     *
     * @param bbm
     * The bbm
     */
    public void setBbm(String bbm) {
        this.bbm = bbm;
    }

    /**
     *
     * @return
     * The whatsapp
     */
    public String getWhatsapp() {
        return whatsapp;
    }

    /**
     *
     * @param whatsapp
     * The whatsapp
     */
    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The line
     */
    public String getLine() {
        return line;
    }

    /**
     *
     * @param line
     * The line
     */
    public void setLine(String line) {
        this.line = line;
    }

    /**
     *
     * @return
     * The smartfren
     */
    public String getSmartfren() {
        return smartfren;
    }

    /**
     *
     * @param smartfren
     * The smartfren
     */
    public void setSmartfren(String smartfren) {
        this.smartfren = smartfren;
    }

    /**
     *
     * @return
     * The facebook
     */
    public String getFacebook() {
        return facebook;
    }

    /**
     *
     * @param facebook
     * The facebook
     */
    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
